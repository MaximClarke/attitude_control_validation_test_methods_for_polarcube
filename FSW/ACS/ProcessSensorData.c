int MAGBinSize,MAGBuffer[4][MAGBinSize], MAG[4], MAGxBias[4], GYRO1[4] GYRO1Bias[4], GYRO2[4], GYRO2Bias[4];
float  BodyRate[4];

% Biasing

MAG[1] -= MAG[1];
MAG[2] -= MAG[2];
MAG[3] -= MAG[3];

GYRO1[1] -= GYRO1Bias[1];
GYRO1[2] -= GYRO1Bias[2];
GYRO1[3] -= GYRO1Bias[3];

GYRO2[1] -= GYRO2Bias[1];
GYRO2[2] -= GYRO2Bias[2];
GYRO2[3] -= GYRO2Bias[3];

% Mean gyro value

BodyRate[1] = (GYRO1[1]+GYRO2[1])/2.0;
BodyRate[2] = (GYRO1[2]+GYRO2[2])/2.0;
BodyRate[3] = (GYRO1[3]+GYRO2[3])/2.0;

% history of magnetometer readings
for (i=1, i<3, i++){
for (j=1, j<MAGBinSize, i++){
MAGBuffer[i][j] = MAGBuffer[i][j-1];
}}

MAGBuffer[1][0] = MAG[1];
MAGBuffer[2][0] = MAG[2];
MAGBuffer[3][0] = MAG[3];

