float RWSpeeds[4], ControlTorques[4], TargetRWSpeeds[4], RWSpeedErrors[4], RWSpeedControlGain;

RWSpeedErrors[1] = RWSpeeds[1] - TargetRWSpeeds[1]; 
RWSpeedErrors[2] = RWSpeeds[2] - TargetRWSpeeds[2]; 
RWSpeedErrors[3] = RWSpeeds[3] - TargetRWSpeeds[3]; 

ControlTorques[1] = -RWSpeedErrors[1] * RWSpeedControlGain;
ControlTorques[2] = -RWSpeedErrors[2] * RWSpeedControlGain;
ControlTorques[3] = -RWSpeedErrors[3] * RWSpeedControlGain;