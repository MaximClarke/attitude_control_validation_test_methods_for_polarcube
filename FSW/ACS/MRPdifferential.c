
float in MRP_1[3], MRP_2[3];
float temp MRP_1_squared, MRP_2_squared, d_MRP_squared, MRP_1_dot_MRP_2
float out d_MRP[3];

MRP_1_squared = MRP_1[0]*MRP_1[0] + MRP_1[1]*MRP_1[1] + MRP_1[2]*MRP_1[2];
MRP_2_squared = MRP_2[0]*MRP_2[0] + MRP_2[1]*MRP_2[1] + MRP_2[2]*MRP_2[2];
MRP_1_dot_MRP_2 = MRP_1[0]*MRP_2[0] + MRP_1[1]*MRP_2[1] + MRP_1[2]*MRP_2[2]

d_MRP[0] = ((1-MRP_2_squared)*MRP_1[0] - (1-MRP_1_squared)*MRP_2[0] + 2 * (MRP_1[1]*MRP_2[2] - MRP_1[2]*MRP_2[1]))...
/ (1 + MRP_2_squared * MRP_1_squared + 2 * MRP_1_dot_MRP_2)

d_MRP[1] = ((1 - MRP_2_squared) * MRP_1[1] - (1 - MRP_1_squared) * MRP_2[1] + 2 * (MRP_1[2] * MRP_2[0] - MRP_1[0] * MRP_2[2]))...
/ (1 + MRP_2_squared * MRP_1_squared + 2 * MRP_1_dot_MRP_2)

d_MRP[2] = ((1-MRP_2_squared)*MRP_1[2]- (1-MRP_1_squared)*MRP_2[2] + 2 * (MRP_1[0]*MRP_2[1] - MRP_1[1]*MRP_2[0]))...
/ (1 + MRP_2_squared * MRP_1_squared + 2 * MRP_1_dot_MRP_2)

d_MRP_squared = d_MRP[0] * d_MRP[0] + d_MRP[0] * d_MRP[0] + d_MRP[0] * d_MRP[0];

if (d_MRP_squared >= 1){
for (i = 0, i<3, i++){
d_MRP[i] = -d_MRP[i]/d_MRP_squared 
}}