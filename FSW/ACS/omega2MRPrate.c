#include vector3D.h

float in MRP, omega;
float temp tempV, MRP_squared_diag, 2_MRP_tilde, 2_MRP_Trans_squared, Multiply_this_by_omega
float out MRP_dot

eye(tempV);
Mmult(1 - dot(MRP,MRP), tempV, MRP_squared_diag);
tilde(MRP, tempV);
Mmult(2, tempV, 2_MRP_tilde);
dotT(MRP,MRP,tempV);
Mmult(2, tempV, 2_MRP_Trans_squared);
Madd(MRP_squared_diag, 2_MRP_tilde, tempV);
Madd(tempV, 2_MRP_Trans_squared, Multiply_this_by_omega);
Mdot(Multiply_this_by_omega, omega, MRP_dot);
MRP_dot = MRP_dot * 0.25;


