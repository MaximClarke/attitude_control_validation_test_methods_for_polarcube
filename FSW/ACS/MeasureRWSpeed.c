int TachometerCounts[4], LastTimestampRWSpeed, dt;
float RWSpeeds[4];

dt = millis - LastTimeStampRWSpeed;
LastTimeStampRWSpeed = millis;

RWSpeed[1] = TachometerCounts[1] / dt * 1000 * 6.283;
RWSpeed[2] = TachometerCounts[2] / dt * 1000 * 6.283;
RWSpeed[3] = TachometerCounts[3] / dt * 1000 * 6.283;

TachometerCounts[1] = 0;
TachometerCounts[2] = 0;
TachometerCounts[3] = 0;