int MAGBinSize,MAGBuffer[4][MAGBinSize]; //triple axis data
int UpAxisIndex, ForwardAxisIndex, UpAxis, ForwardAxis[4], LeftAxis[4], ACC[4], sign; 
unsigned long Timestamp, PreviousTimestamp;
float magnitude, MeasuredHeading, PredictedHeading, PCHeading, Pitch, Roll, dueWEST[4], FWcrossGRAV[4], adjMAGx, adjMAGy, adjMAGz, GYROdotGRAV;
float MagneticField[4] = {0, 0, 0, 0};


  for (i=0, i<MAGBinSize, i++){ 
  MagneticField[1] =+ MAGBuffer[1][i];
  MagneticField[2] =+ MAGBuffer[2][i];
  MagneticField[3] =+ MAGBuffer[3][i];
  }
  
  MagneticField[1] \= MAGBinSize;
  MagneticField[2] \= MAGBinSize;
  MagneticField[3] \= MAGBinSize;

  // Calculate magnetic west //Cross Product
  dueWEST[1] = MagneticField[2] * ACC[3] - MagneticField[3] * ACC[2];
  dueWEST[2] = MagneticField[3] * ACC[1] - MagneticField[1] * ACC[3];
  dueWEST[3] = MagneticField[1] * ACC[2] - MagneticField[2] * ACC[1];
  
  //Normalize
  magnitude = sqrt(sq(dueWEST[1]) + sq(dueWEST[2]) + sq(dueWEST[3]));

  dueWEST[1] /= magnitude;
  dueWEST[2] /= magnitude;
  dueWEST[3] /= magnitude;

  //Normalize
  magnitude = sqrt(sq(ACC[1]) + sq(ACC[2]) + sq(ACC[3]));

  Gravity[1] = -ACC[1]/magnitude;
  Gravity[2] = -ACC[2]/magnitude;
  Gravity[3] = -ACC[2]/magnitude;
  
  //determine which way is up
  
  if (abs(GRAVvec[1]) > abs(GRAVvec[2]) && abs(GRAVvec[1]) > abs(GRAVvec[3])){
	UpAxisIndex = 1;
	}
	else if(abs(GRAVvec[2]) > abs(GRAVvec[1]) && abs(GRAVvec[2]) > abs(GRAVvec[3])){
	UpAxisIndex = 2;
	}
	else{
	UpAxisIndex = 3;
	}
	
	UpAxis = {0, 0, 0, 0}
	sign = 1;
	
  if (Gravity[UpAxisIndex] > 0){
  sign = -1;
  }
  
  UpAxis[UpAxisIndex] = sign;
  
  ForwardAxis = {0, 0, 0, 0};
  
  
  
  
  if (UpAxis[1]==0) ForwardAxisIndex = 1;
  else ForwardAxisIndex = 2;
  
  ForwardAxis[ForwardAxisIndex] = 1;
  
  
  
  
 LeftAxis = {0, 0, 0};
 
if (UpAxisIndex == 1){
LeftAxis[3] = sign;
}
else if (UpAxisIndex == 2) {
LeftAxis[3] = -sign;
}
else{
LeftAxis[2] = sign;
}
  
  // cross product of body forward axis and gravity vector
  FWcrossGRAV[1] = ForwardAxis[2] * Gravity[3] - ForwardAxis[3] * Gravity[2];
  FWcrossGRAV[2] = ForwardAxis[3] * Gravity[1] - ForwardAxis[1] * Gravity[3];
  FWcrossGRAV[3] = ForwardAxis[1] * Gravity[2] - ForwardAxis[2] * Gravity[1];

  magnitude = sqrt(sq(FWcrossGRAV[1]) + sq(FWcrossGRAV[2]) + sq(FWcrossGRAV[3]));
  
  // Calculate pitch
  Pitch = acos(magnitude);

  if ((Gravity[ForwardAxisIndex]*sign) > 0) {
    Pitch = -Pitch;
  }

  // Normalize
  FWcrossGRAV[1] /= magnitude;
  FWcrossGRAV[2] /= magnitude;
  FWcrossGRAV[3] /= magnitude;


  // Calculate MeasuredHeading
 MeasuredHeading = acos(dueWEST[1] * FWcrossGRAV[1] + dueWEST[2] * FWcrossGRAV[2]+ dueWEST[3] * FWcrossGRAV[3]);
  if (dueWEST[ForwardAxisIndex] < 0) {
    MeasuredHeading = -MeasuredHeading;
  }
  
  if (PredictedHeading*MeasuredHeading < 0){
  PCHeading = PredictedHeading;
 }
 else{
 PCHeading = PredictedHeading*0.99+MeasuredHeading*0.01;
}
if (PCHeading > 3.141592){
 PCHeading = PCHeading-6.2831;
}
if (PCHeading < -3.141592){
  PCHeading = PCHeading+6.2831;
}

PredictedHeading = PCHeading;

  //Calculate roll
 Roll = acos( LeftAxis[0]*FWcrossGRAV[0]+LeftAxis[1]*FWcrossGRAV[1]+LeftAxis[2]*FWcrossGRAV[2]);

  if (( UpAxis[0]*FWcrossGRAV[0]+UpAxis[1]*FWcrossGRAV[1]+UpAxis[2]*FWcrossGRAV[2]) < 0) {
    Roll = -Roll;
  }