float TargetHeading, PCHeading, Gravity[4], HeadingError, ErrorMRP[4], magnitude;
int ACC[4];

magnitude = sqrt(sq(ACC[1]) + sq(ACC[2]) + sq(ACC[3]));

Gravity[1] = -ACC[1]/magnitude;
Gravity[2] = -ACC[2]/magnitude;
Gravity[3] = -ACC[2]/magnitude;

HeadingError = TargetHeading - PCHeading;

ErrorMRP[1] = -Gravity[1] * tan(HeadingError/4);
ErrorMRP[2] = -Gravity[2] * tan(HeadingError/4);
ErrorMRP[3] = -Gravity[3] * tan(HeadingError/4);