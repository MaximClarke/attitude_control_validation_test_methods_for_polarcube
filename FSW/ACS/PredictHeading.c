float PredictedHeading, Gravity[4], MeanGyro[4], HeadingRate, magnitude;
int ACC[4], LastPredictionTimestamp, TimeStep;
 
magnitude = sqrt(sq(ACC[1]) + sq(ACC[2]) + sq(ACC[3]));


Gravity[1] = -ACC[1]/magnitude;
Gravity[2] = -ACC[2]/magnitude;
Gravity[3] = -ACC[2]/magnitude;

HeadingRate = -MeanGyro[1]*Gravity[1]-MeanGyro[2]*Gravity[2]-MeanGyro[3]*Gravity[3];

TimeStep = millis - LastPredictionTimestamp;
LastPredictionTimestamp = millis;

PredictedHeading = PredictedHeading + TImestep*HeadingRate;



