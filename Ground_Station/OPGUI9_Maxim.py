

##ACS Testing GUI
##
##This program creates a GUI that is capable of both sending and
##receiving over a serial port using two separate threads. It
##allows a user to initiate a test with CDH as well as end it.
##The information is stored into a text file in the same directory
##at the conclussion of each test.


from tkFileDialog import *
import struct
import subprocess
import threading
import serial
import sys
import time
import Tkinter as tk
import Queue
from Tkinter import *
from multiprocessing import Process, Queue



#################################
########Initial Setup############
#################################

inputPort = raw_input("Which Serial Port?") 
XbeePort = serial.Serial("/dev/ttyUSB"+inputPort, 115200,timeout=None)
identifier = ""


OPLibrary = [
  ['1','Set Mode', 'CTL Mode X','CTL Mode Y','CTL Mode Z'],
  ['2','Set Mag Bias', 'Mag X', 'Mag Y', 'Mag Z'],
  ['3','Set RWS Targets', 'Target Speed X', 'Target Speed Y', 'Target Speed Z'],
  ['4','Set PID Gains', 'K', 'Px', 'Py', 'Pz', 'Kix', 'Kiy', 'Kiz'],
  ['5','Set Gyro Bias', 'Gryo1 X', 'Gryo1 Y', 'Gryo1 Z', 'Gyro2 X', 'Gyro2 Y', 'Gyro2 Z'],
  ['6','Set Targets', 'Rate', 'Heading'],
  ['7','Set Filter Mode', 'Filter Mode']
]

PastLibrary=[['']]

Options = [['Sensors', 'Mag X', 'Mag Y', 'Mag Z', 'Acc X', 'Acc Y', 'Acc Z', 'Gyro1 X', 'Gyro1 Y', 'Gyro1 Z', 'Gyro2 X', 'Gyro2 Y', 'Gyro2 Z'],
           ['RW Speeds', 'X', 'Y', 'Z'],
           ['Control Law Gains', 'K', 'KIX', 'KIY', 'KIZ', 'PX', 'PY', 'PZ'],
           ['Control Law Targets', 'Rate', 'Heading', 'RWS X', 'RWS Y', 'RWS Z'],
           ['Bias Values', 'Mag X', 'Mag Y', 'Mag Z', 'Gyro1 X', 'Gyro1 Y', 'Gyro1 Z', 'Gyro2 X', 'Gyro2 Y', 'Gyro2 Z'],
           ['Motor Override Mode', 'CTL Mode X','CTL Mode Y','CTL Mode Z'],
           ['Attitude', 'MRP X', 'MRP Y', 'MRP Z','Error MRP X', 'Error MRP Y', 'Error MRP Z'],
           ['Mag Filter Mode', 'Filter Mode'],
           ['Attitude', 'Heading','Pitch','Roll'],
           ['Commanded Torques', 'X','Y','Z']
]




#######################################
######Receiving Window/Thread##########
#######################################

def readSerial():

    ###Window Setup###
    #Initially defines content and format.
    def App():
        global txtTemp, txtAttitude, txtPitch, dataDict, textBoxes
        win2 = Tk()
        win2.geometry("1800x900")
        win2.config(background="grey")
        frameHeader = Frame(win2,padx=30,pady=30,background="grey")
        Label(frameHeader, text="ACS Test Log", pady=20, font='Helvetica 25',bd=10,background="grey").pack(side=TOP)
        frameHeader.pack()
        
        frameMain = Frame(win2,padx=2,pady=2,background="#666666")
        dataDict= {}
        textBoxes = []
        
 
        for i in range(0,len(Options)):
            bgFrame = Frame(frameMain,pady=5,padx=5,background="#CCCCCC")
            bgFrame.grid(row=i%2,column=i/2,sticky=N+W+E+S)
            tempFrame = LabelFrame(bgFrame, text=Options[i][0],background="#CCCCCC")
            tempFrame.pack(expand=YES,fill=BOTH)
            for j in range(1,len(Options[i])):
                Label(tempFrame, text=Options[i][j],padx=5,pady=5,font='Helvetica 10',background="#CCCCCC",width=15).grid(sticky=E,row=(j*2),column=0)
                textBoxes.append(Text(tempFrame, height=1, width=25,wrap='word', font='Helvetica 10',state="disabled",background="#CCCCCC"))
                textBoxes[len(textBoxes)-1].grid(row=(j*2), column=1,sticky=W,columnspan=2)
            
            
     
        frameMain.pack()

        
        return win2




    ###Receiving Loop###
    #Reads line from stdin. Depending on whether active is True or
    #False, data will either be sent to display or sent to file.
    def receive():
        global active, identifier, textBoxes, filestatus, txtTerminal
        dataIn=""
        if active==True:
            XbeePort.flushInput()
            dataIn = XbeePort.read(213)
            dataIn = struct.unpack("f" *53 + "c", dataIn)
            print dataIn
            if dataIn[53]=='U':
                for i in range(0,52):
                    textBoxes[i].config(state="normal")
                    textBoxes[i].delete(1.0,'end')
                    textBoxes[i].insert('end', dataIn[i])
                    textBoxes[i].config(state="disabled")
                txtTerminal.config(state="normal")
                txtTerminal.insert(END, "Packet Received!\n")
                txtTerminal.see(END) 
                #txtTerminal.config(state="disabled")
            else:
                txtTerminal.config(state="normal")
                txtTerminal.insert(END, "Packet not found!\n")
                txtTerminal.see(END)
                #print str(dataIn[45])
                #print dataIn[45]
                #txtTerminal.config(state="disabled")
        elif active==False and filestatus==True:
            txtTerminal.config(state="normal")
            txtTerminal.insert(END, "Writing File...\n")
            txtTerminal.see(END)
            #txtTerminal.config(state="disabled")
            
            fp = open(identifier, 'w')
            fileData=""

            dataIn = XbeePort.read()
            while dataIn!="S":
                fileData+=dataIn
                dataIn = XbeePort.read()
            fp.write(fileData)
            filestatus=False
            txtTerminal.config(state="normal")
            txtTerminal.insert(END, "Finished Streaming. Data written to file: " + identifier + "\n")
            txtTerminal.see(END)
            #txtTerminal.config(state="disabled")
            
        app.after(100, receive)


    app = App()
    
    app.after(100, receive)
    app.mainloop()


        

#######################################
#####Transmitting Window/Thread########
#######################################



###Window Setup###
#Initially defines content and format.

def makeWindow () :
    global functionVar, parameterVar, listFunctionSel, parameterSelect, parameter, PastIndex, PastLibrary, \
        listPastSel, btnStart, scrlFunctionSel, file_string, lblFile,txtTerminal, txtFile, btnACS, btnFrang
    
    PastIndex = 0

    win = Tk()
    win.geometry("1500x800")
    win.resizable(width=FALSE, height=FALSE)
    win.title("Status: Idle")
    win.configure(background="grey")
    frmMain = Frame(win,bg="#666666")
    frmMain.pack(padx=30,pady=30)
    
    #Past Selection Scroll Box
    frmPastSel = Frame(frmMain,width=200)
    frmPastSel.grid(row=1,column=0,sticky=N+W+E,pady=(0,2),padx=(2,0),columnspan=2)
    lblfrmPastSel = LabelFrame(frmPastSel, text="Select Command",width=200)
    lblfrmPastSel.pack(padx=20,pady=10,ipady=10,expand=YES,fill=BOTH)

    scrlFunctionSel = Scrollbar(lblfrmPastSel, orient=VERTICAL)
    listFunctionSel = Listbox(lblfrmPastSel, yscrollcommand=scrlFunctionSel.set, height=8, width=17)
    scrlFunctionSel.config (command=listFunctionSel.yview)
    listFunctionSel.pack(side=LEFT,padx=(15,0))
    scrlFunctionSel.pack(fill=Y, side=LEFT,pady=10,padx=(0,15))
 
 
    scrlPastSel = Scrollbar(lblfrmPastSel, orient=VERTICAL)
    listPastSel = Listbox(lblfrmPastSel, yscrollcommand=scrlPastSel.set, height=8,width=81)
    scrlPastSel.config (command=listPastSel.yview)
    listPastSel.pack(side=LEFT)
    scrlPastSel.pack(side=LEFT, fill=Y,pady=10)

    btnSelect = Button(lblfrmPastSel,text="Select",command=Select)
    btnSelect.pack(side=LEFT, padx=10,pady=5)

    
    for i in range(0,len(OPLibrary)):   
        listFunctionSel.insert(END, OPLibrary[i][1])  #Populate scroll box
    

    
    #File Selection
    frmFile = Frame(frmMain)
    frmFile.grid(row=0,column=1,sticky=N+W+E+S,pady=(2,0))
    lblfrmFile = LabelFrame(frmFile, text="Stream Test Data",width=200)
    lblfrmFile.pack(pady=(20,0),padx=20, expand=YES,fill=BOTH)
    
    lblFileName = Label(lblfrmFile, text="Select File:")
    txtFile = Text(lblfrmFile,font='Helvetica 12',height=1,width=30,state="disabled")
    btnFile = Button(lblfrmFile, text="Browse...", command=file_select)
    btnStream = Button(lblfrmFile, text="Stream File", command=Stream)
    lblFileName.pack(padx=5,pady=10,side=LEFT)
    txtFile.pack(pady=10,side=LEFT)
    btnFile.pack(pady=10,padx=(0,50),side=LEFT)
    btnStream.pack(padx=10,pady=10,side=RIGHT,expand=YES)
                   
                             
    #Button Display
    btnDisplay = Frame(frmMain)
    btnDisplay.grid(row=0,column=0,sticky=N+W+E+S,pady=(2,0),padx=(2,0))
    btnGroup = LabelFrame(btnDisplay, text="Start/Stop Testing")
    btnGroup.pack(expand=YES,fill=BOTH,padx=20,pady=(20,0))
    
    btnStart = Button(btnGroup,text="Start",command=Start, bg='grey', fg='black', bd=2)
    btnStop = Button(btnGroup,text="Stop",command=Stop)
    btnACS = Button(btnGroup,text="ACS",command=ACS, bg='grey', fg='black', bd=2)
    btnFrang = Button(btnGroup,text="Frangibolt",command=Frang, bg='grey', fg='black', bd=2)

    
    btnStart.pack(side=LEFT, padx=10,pady=5,expand=YES)
    btnACS.pack(side=RIGHT, padx=10,pady=5,expand=YES)
    btnFrang.pack(side=RIGHT, padx=10,pady=5,expand=YES)
    btnStop.pack(side=RIGHT, padx=10,pady=5,expand=YES)

    #Terminal Display
    frmTerminal = Frame(frmMain)
    frmTerminal.grid(row=0,column=2,rowspan=2,sticky=N+S+W+E,pady=2,padx=(0,2))
    lblfrmTerminal = LabelFrame(frmTerminal, text="Terminal")
    lblfrmTerminal.pack(fill=BOTH,expand=YES,padx=20,pady=10)
    scrlTerminal = Scrollbar(lblfrmTerminal, orient=VERTICAL)
    txtTerminal = Text(lblfrmTerminal, yscrollcommand=scrlTerminal.set, height=6,font='Helvetica 11',state="disabled",width=40)
    scrlTerminal.config (command=txtTerminal.yview)
    scrlTerminal.pack(fill=Y,side=RIGHT,padx=(0,8),pady=8)
    txtTerminal.pack(side=LEFT,expand=YES,fill=BOTH,pady=8,padx=(8,0))
    #Grid.columnconfigure(win, 0, weight=1)
    #Grid.columnconfigure(win, 1, weight=1)

    return win



#######################################
###########Button Functions############
#######################################






###File Select###
def file_select():
    global identifier, file_string, lblFile,txtTerminal, txtFile
    identifier = asksaveasfilename(defaultextension=".txt",filetypes=(("text file", "*.txt"),("All Files", "*.*")))
    txtFile.config(state="normal")
    txtFile.delete("1.0",END)
    txtFile.insert(END,identifier)
    txtFile.config(state="disabled")



###Submit Button Action###
#Depending on whether a past function or new function is
#selected, the size of the selected function array will be
#determined and then the parameters will be stored into history
#and put into 'inputParameters'.
def Submit () : 
    global PastIndex, PastLibrary, listPastSel, currentSelection, currentSel
    inputParameters =""
    inputParamTest=""
    formatstr=""
    if currentSel==True:    
        indexSize = len(OPLibrary[currentSelection])-2
        PastLibrary[PastIndex][0] = OPLibrary[currentSelection][0]
        PastLibrary[PastIndex].append(OPLibrary[currentSelection][1])
        inputParamTest+=OPLibrary[currentSelection][0] + " "
    elif currentSel==False:  
        indexSize = (len(PastLibrary[currentSelection])-2)/2
        PastLibrary[PastIndex][0] = PastLibrary[currentSelection][0]
        PastLibrary[PastIndex].append(PastLibrary[currentSelection][1])
        inputParamTest+=PastLibrary[currentSelection][0] + " "
    

    
    
    for i in range(0,indexSize):
        tempParam = parameterVar[i].get()
        try:
            float(tempParam)
            
        except:
            txtTerminal.config(state="normal")
            txtTerminal.insert(END, "Ensure all fields are filled with integers!\n")
            txtTerminal.see(END)
            txtTerminal.config(state="disabled")
            return
        if currentSel==True:    
            PastLibrary[PastIndex].append(OPLibrary[currentSelection][i+2])
            PastLibrary[PastIndex].append(float(tempParam))
        elif currentSel==False:
            PastLibrary[PastIndex].append(PastLibrary[currentSelection][(2*i)+2])
            PastLibrary[PastIndex].append(float(tempParam))
        inputParamTest+=tempParam + " "
    txtTerminal.config(state="normal")
    txtTerminal.insert(END, "Op Code Sent: " + inputParamTest + "\n")
    txtTerminal.see(END)
    txtTerminal.config(state="disabled")
    XbeePort.write(inputParamTest + "\n")
    

    tempstr="OP: "
    for i in range(0,len(PastLibrary[PastIndex])):
        tempstr+=str(PastLibrary[PastIndex][i]) + " "
        if i%2>0 or i==0:
            tempstr += "  "
        elif i%2==0 and i>0:
            tempstr += " = "
    listPastSel.insert(END, tempstr)#PastLibrary[PastIndex])
    listPastSel.see(END)
    PastIndex=PastIndex+1
    PastLibrary.append([[]])


###Select Button Action###
#Dynamically creates labels and entry boxes depending
#on which function is currently selected.
def Select () :
    
    global frameDyn, parameterVar, parameter, currentSelection, currentSel,lblfrmDyn

    if len(listFunctionSel.curselection())>0:
        currentSel=True
        currentSelection=int(listFunctionSel.curselection()[0])
    elif len(listPastSel.curselection())>0:
        currentSel=False
        currentSelection=int(listPastSel.curselection()[0])
        
    
    frameDyn.destroy()
    frameDyn = Frame(win,background="#666666")
    frameDyn.pack()
    lblfrmDyn = LabelFrame(frameDyn, text="")
    
    
    parameterVar =['']
    parameter = ['']


    if currentSel==True:
        lblfrmDyn.config(text=OPLibrary[currentSelection][1])
        indexSize = len(OPLibrary[currentSelection])-2

    elif currentSel==False:
        indexSize = (len(PastLibrary[currentSelection])-2)/2
        lblfrmDyn.config(text=PastLibrary[currentSelection][1])


    for i in range(0,indexSize):
        parameterVar[i] = StringVar()
        if currentSel==True:
            Label(lblfrmDyn, text=OPLibrary[currentSelection][i+2],wraplength=71).grid(row=2+2*(i/5), column=i%5, sticky=N,pady=5)

        elif currentSel==False:
            Label(lblfrmDyn, text=PastLibrary[currentSelection][(2*i)+2],wraplength=71).grid(row=2+2*(i/5), column=i%5, sticky=N,pady=5)
            parameterVar[i].set(PastLibrary[currentSelection][(2*i)+3])



        parameter[i] = Entry(lblfrmDyn, textvariable=parameterVar[i],justify=CENTER, width = 15)
        parameter[i].grid(row=3+2*(i/5), column=i%5, sticky=N,padx=10,pady=10)
        parameterVar.append([])
        parameter.append([])
        
    lblfrmDyn.pack(pady=2,padx=2)
    btnSubmit = Button(frameDyn, text="Submit",command=Submit)
    btnSubmit.pack(side=LEFT, padx=10,pady=5,expand=YES)
    
    
###Stream Button Action###
def Stream():
    global active, btnStream, identifier, filestatus
    if identifier=="":
        txtTerminal.config(state="normal")
        txtTerminal.insert(END, "Please Select a Folder!\n")
        txtTerminal.see(END)
        txtTerminal.config(state="disabled")
        return
    if active==True:
        txtTerminal.config(state="normal")
        txtTerminal.insert(END, "Cannot Stream during Test!\n")
        txtTerminal.see(END)
        txtTerminal.config(state="disabled")
        return
    XbeePort.flushInput()
    XbeePort.write("9\n")
    filestatus=True
    print "Streaming File..."
    

def ACS():
    global activeACS, btnACS, scrlFunctionSel

    if activeACS==False:
        activeACS=True
        XbeePort.write("a\n")
        txtTerminal.config(state="normal")
        txtTerminal.insert(END, "ACS Activated!\n")
        txtTerminal.see(END)
        btnACS.config(bg='red', fg='black', bd=2)
    else:
        activeACS=False
        XbeePort.write("b\n")
        txtTerminal.config(state="normal")
        txtTerminal.insert(END, "ACS Deactivated!\n")
        txtTerminal.see(END)
        btnACS.config(bg='grey', fg='black', bd=2)

def Frang():
    global activeFrang, btnFrang, scrlFunctionSel

    if activeFrang==False:
        activeFrang=True
        XbeePort.write("c\n")
        txtTerminal.config(state="normal")
        txtTerminal.insert(END, "Frangibolt Activated!\n")
        txtTerminal.see(END)
        btnFrang.config(bg='red', fg='black', bd=2)
    else:
        activeFrang=False
        XbeePort.write("d\n")
        txtTerminal.config(state="normal")
        txtTerminal.insert(END, "Frangibolt Deactivated!\n")
        txtTerminal.see(END)
        btnFrang.config(bg='grey', fg='black', bd=2)







###Start Button Action###
#Sends a preconfigured ACP Packet to tell CDH to
#begin testing
def Start():
    global active, btnStart, scrlFunctionSel
    active=True
    Status()
    XbeePort.write("8\n")
    txtTerminal.config(state="normal")
    txtTerminal.insert(END, "Testing Initiated\n")
    txtTerminal.see(END)
    txtTerminal.config(state="disabled")
    btnStart.config(state=DISABLED, bg='red', fg='black', bd=2)
    



###Stop Button Action###
#Sends a preconfigured ACP Packet to tell CDH to
#end testing
def Stop():
    global fp, active, identifier, btnStart
    active=False
    Status()
    #callback("8 0 3 3 3")
    XbeePort.write("e\n")
    txtTerminal.config(state="normal")
    txtTerminal.insert(END, "Testing Stopped\n")
    txtTerminal.see(END)
    txtTerminal.config(state="disabled")
    btnStart.config(state='normal', bg='grey', fg='black', bd=2)
    





#######################################
###############Functions###############
#######################################

###Call Subprocess###
#Calls a specified subprocess with arguments of 'inputparameters'.
#Then reads lines from stdout, stores onto 'output' and sends to
#serial port.
##def callback(inputParameters):
##    print inputParameters
##    process = subprocess.Popen("/home/fsw/CPP/workspace/PyBee/Debug/PyBee " + inputParameters, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
##    count=0
##    output = ""
##    while True:
##        buff = process.stdout.readline()
##        output = output + buff
##        if buff == '':    
##            count += 1
##
##        if buff == '' and process.poll() != None: 
##            break
##
##    process.wait()
##    print output
##    XbeePort.write(output + "\n")



###Window Title###
#Changes the transmitting window's title depending
#on the status of testing.
def Status () :
    global active

    win.title("")
    if active==True:
        win.title("Status: Test In Progress")
    else:
        win.title("Status: Idle")







win = makeWindow()
frameDyn = Frame(win)
frameDyn.pack()

thread1 = threading.Thread(target=readSerial)
thread1.start()
active=False
activeACS=False
activeFrang=False
filestatus=False
win.mainloop()

