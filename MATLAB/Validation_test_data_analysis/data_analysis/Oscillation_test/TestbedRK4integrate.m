% RK4integrate.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% RK4integrate.m performs a Runge-Kutta 4 integration of rigid body dynamics
% for reference: http://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods

function [new_omega,new_sigma] = TestbedRK4integrate(time_step,omega,sigma,Omega,torques,I,J_w,BFRotationAxis)
[omega_dot_k1, sigma_dot_k1] = TestbedEOM(omega,sigma,Omega,torques,I,J_w,BFRotationAxis);
[omega_dot_k2, sigma_dot_k2] = TestbedEOM(omega + 0.5 * time_step * omega_dot_k1,sigma + 0.5 * time_step * sigma_dot_k1,Omega,torques,I,J_w,BFRotationAxis);
[omega_dot_k3, sigma_dot_k3] = TestbedEOM(omega + 0.5 * time_step * omega_dot_k2,sigma + 0.5 * time_step * sigma_dot_k2,Omega,torques,I,J_w,BFRotationAxis);
[omega_dot_k4, sigma_dot_k4] = TestbedEOM(omega + time_step * omega_dot_k3,sigma + time_step * sigma_dot_k3,Omega,torques,I,J_w,BFRotationAxis);
new_omega = omega + time_step / 6 * ( omega_dot_k1 + 2 * omega_dot_k2 + 2 * omega_dot_k3 + omega_dot_k4);
new_sigma = sigma + time_step / 6 * ( sigma_dot_k1 + 2 * sigma_dot_k2 + 2 * sigma_dot_k3 + sigma_dot_k4);
end