% StringTestbedModelFN.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% StringTestbedModelFN.m is executable version of StringTestbedModel.m. It
% performs the same actions (dynamic model of the test model on the string
% suspension testbed) it has been optimized for speed and does not store
% state variables.

function [simtime,theta] = StringTestbedModelFN(InitialTheta,TargetTheta,K,P,Ki,IB,SpringConstant, DampingConstant, t)

% Timestamp
dt = 0.05;
simtime = dt:dt:t;
outputlength = t/dt;

% Moment of inertia of the reaction wheels
J_RW=1.12e-5;



% Define the roation axis in the body frame
BFRotationAxis = [0; 0; 1];
BFRotationAxis = BFRotationAxis/norm(BFRotationAxis);

% Empty vector
theta = zeros(1,outputlength);


% Initial conditions
omegaBB = BFRotationAxis*0; % rotation can only occur about the rotation axis
omegaRW = [0; 0; 0];
MRP = BFRotationAxis*tan(InitialTheta/4);
theta(:,1) = InitialTheta;
sigInt = [0; 0; 0];
target_sigma = BFRotationAxis*tan(TargetTheta/4);
target_omega = [0; 0; 0];
target_omegaDot = [0; 0; 0];
d_omega0 = omegaBB - target_omega;


for i=2:length(simtime)
    
    % Torques from testbed
    Tstring = -SpringConstant*(theta(i-1))* BFRotationAxis;
    Tdamping = -DampingConstant*(omegaBB);
    
    % Error terms
    ErrorMRP = diff_sigma(MRP,target_sigma);
    d_omega = omegaBB - target_omega;
    
    % Error integrator
    sigInt = sigInt + ErrorMRP*dt;
    zeta = K*sigInt + IB*(d_omega - d_omega0);
    
    % Control law (Motor output torque)
     TControl = -IB*(target_omegaDot - CrossMatrix(omegaBB)*target_omega) + K*ErrorMRP + P*d_omega + P*Ki*zeta...
        - CrossMatrix(omegaBB)*(IB*omegaBB + J_RW*(omegaRW+omegaBB));
     
    % Sum of torques      
    torques = Tstring+Tdamping-TControl;
    
    % Rigid body dynamics
    [omegaBB,MRP] = TestbedRK4integrate(dt,omegaBB,MRP,omegaRW,torques,IB,J_RW,BFRotationAxis);
    
    % Integrate reaction wheel speed
    omegaRW = omegaRW+dt/J_RW*TControl;
    
    % Update deflection angle
    theta(i) = 4*atan(norm(MRP))*sign(MRP(3));
end

end



