clear all
load 'Friction_data.mat'
load 'Output_data.mat'


all_the_friction_torques = [];
all_the_friction_speeds = [];

all_the_output_torques = [];
all_the_output_speeds = [];

for i = length(FrictionData)
    all_the_friction_torques = [all_the_friction_torques; FrictionData(i).Torque_x' FrictionData(i).Torque_y' FrictionData(i).Torque_z'];
    all_the_friction_speeds = [all_the_friction_speeds; FrictionData(i).Reference_RWS_x' FrictionData(i).Reference_RWS_y' FrictionData(i).Reference_RWS_z'];  
end

for i = length(FrictionData)
    all_the_output_torques = [all_the_output_torques; OutputData(i).Torque_x' OutputData(i).Torque_y' OutputData(i).Torque_z'];
    all_the_output_speeds = [all_the_output_speeds; OutputData(i).Reference_RWS_x' OutputData(i).Reference_RWS_y' OutputData(i).Reference_RWS_z'];  
end

friction_fit_torque_x = fit(all_the_friction_speeds(:,1), all_the_friction_torques(:,1),'poly1');
friction_fit_torque_y = fit(all_the_friction_speeds(:,2), all_the_friction_torques(:,2),'poly1');
friction_fit_torque_z = fit(all_the_friction_speeds(:,3), all_the_friction_torques(:,3),'poly1');

output_fit_torque_x = fit(all_the_output_speeds(:,1), all_the_output_torques(:,1),'poly1');
output_fit_torque_y = fit(all_the_output_speeds(:,2), all_the_output_torques(:,2),'poly1');
output_fit_torque_z = fit(all_the_output_speeds(:,3), all_the_output_torques(:,3),'poly1');

friction_confidence_x = confint(friction_fit_torque_x);
friction_confidence_y = confint(friction_fit_torque_y);
friction_confidence_z = confint(friction_fit_torque_z);

friction_confidence_bounds(1,:) = [(friction_confidence_x(1,1)-friction_confidence_x(2,1))/2 (friction_confidence_x(1,2)-friction_confidence_x(2,2))/2];
friction_confidence_bounds(2,:) = [(friction_confidence_y(1,1)-friction_confidence_y(2,1))/2 (friction_confidence_y(1,2)-friction_confidence_y(2,2))/2];
friction_confidence_bounds(3,:) = [(friction_confidence_z(1,1)-friction_confidence_z(2,1))/2 (friction_confidence_z(1,2)-friction_confidence_z(2,2))/2];

output_confidence_x = confint(output_fit_torque_x);
output_confidence_y = confint(output_fit_torque_y);
output_confidence_z = confint(output_fit_torque_z);

output_confidence_bounds(1,:) = [(output_confidence_x(1,1)-output_confidence_x(2,1))/2 (output_confidence_x(1,2)-output_confidence_x(2,2))/2];
output_confidence_bounds(2,:) = [(output_confidence_y(1,1)-output_confidence_y(2,1))/2 (output_confidence_y(1,2)-output_confidence_y(2,2))/2];
output_confidence_bounds(3,:) = [(output_confidence_z(1,1)-output_confidence_z(2,1))/2 (output_confidence_z(1,2)-output_confidence_z(2,2))/2];


netZeroValue_x = output_fit_torque_x.p2 - friction_fit_torque_x.p2; 
netZeroValue_y = output_fit_torque_y.p2 - friction_fit_torque_y.p2; 
netZeroValue_z = output_fit_torque_z.p2 - friction_fit_torque_z.p2; 

netSlope_x = output_fit_torque_x.p1 - friction_fit_torque_x.p1;
netSlope_y = output_fit_torque_y.p1 - friction_fit_torque_y.p1;
netSlope_z = output_fit_torque_z.p1 - friction_fit_torque_z.p1;

SampleRWS_x = 0:1:160;
SampleRWS_y = 0:1:140;
SampleRWS_z = 0:1:100;

TorqueSum_x = netZeroValue_x + netSlope_x*SampleRWS_x; 
TorqueSum_y = netZeroValue_y + netSlope_y*SampleRWS_y; 
TorqueSum_z = netZeroValue_z + netSlope_z*SampleRWS_z; 

OutputTorque_x = output_fit_torque_x.p2 + output_fit_torque_x.p1*SampleRWS_x;
OutputTorque_y = output_fit_torque_y.p2 + output_fit_torque_y.p1*SampleRWS_y;
OutputTorque_z = output_fit_torque_z.p2 + output_fit_torque_z.p1*SampleRWS_z;

figure(1)
subplot(3,1,1)
plot(SampleRWS_x,OutputTorque_x,SampleRWS_x,TorqueSum_x)
title({'Reaction wheel torque outputs (blue) and net "output+friction" torque (green)'; 'plotted against reaction wheel speeds'})
ylabel('X reaction wheel torque (Nm)')
subplot(3,1,2)
plot(SampleRWS_y,OutputTorque_y,SampleRWS_y,TorqueSum_y)
ylabel('Y reaction wheel torque (Nm)')
subplot(3,1,3)
plot(SampleRWS_z,OutputTorque_z,SampleRWS_z,TorqueSum_z)
ylabel('Z reaction wheel torque (Nm)')
xlabel('Reaction wheel speed (rad/s)')