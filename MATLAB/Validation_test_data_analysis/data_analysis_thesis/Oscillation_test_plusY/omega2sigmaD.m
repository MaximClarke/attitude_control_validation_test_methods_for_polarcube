% omega2sigmaD.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% omega2sigmaD.m calculates the rate change in the Modified Rodriguez
% Parameter describing attitude given the current MRP and body axis rotation
% rates
function [sigma_dot] = omega2sigmaD(omega,sigma)
sigma_dot = 0.25 * ((1-sigma' * sigma) * eye(3)...
    + 2 * CrossMatrix(sigma) + 2 * (sigma * sigma')) * omega;