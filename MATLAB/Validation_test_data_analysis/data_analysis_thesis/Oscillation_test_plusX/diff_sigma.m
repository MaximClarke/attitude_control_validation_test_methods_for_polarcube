% diff_sigma.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% diff_sigma.m calculates the modified rodriguez parameter that represents
% the rotational difference between two MRP vectors.

function d_sigma = diff_sigma(sigma1,sigma2)
d_sigma = ((1 - (sigma2' * sigma2)) * sigma1 - (1 - (sigma1' * sigma1)) * sigma2 + 2 * CrossMatrix(sigma1) * sigma2)...
    / (1 + (sigma2' * sigma2) * (sigma1' * sigma1) + 2 * sigma2' * sigma1);
% Convert to shadow MRP if necessary
if d_sigma' * d_sigma >= 1
    d_sigma = -d_sigma ./ (d_sigma' * d_sigma);
end