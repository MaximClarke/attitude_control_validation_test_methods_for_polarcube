clear all
load 'Friction_data.mat'
load 'Output_data.mat'

for i = 1:length(FrictionData)
    FrictionSlopeValues_x(i) = FrictionData(i).Fit_Torque_x.p1;
    FrictionZeroValues_x(i) = FrictionData(i).Fit_Torque_x.p2;
    
    FrictionSlopeValues_y(i) = FrictionData(i).Fit_Torque_y.p1;
    FrictionZeroValues_y(i) = FrictionData(i).Fit_Torque_y.p2;
    
    FrictionSlopeValues_z(i) = FrictionData(i).Fit_Torque_z.p1;
    FrictionZeroValues_z(i) = FrictionData(i).Fit_Torque_z.p2;
end

for i = 1:length(OutputData)
    OutputSlopeValues_x(i) = OutputData(i).Fit_Torque_x.p1;
    OutputZeroValues_x(i) = OutputData(i).Fit_Torque_x.p2;
    
    OutputSlopeValues_y(i) = OutputData(i).Fit_Torque_y.p1;
    OutputZeroValues_y(i) = OutputData(i).Fit_Torque_y.p2;
    
    OutputSlopeValues_z(i) = OutputData(i).Fit_Torque_z.p1;
    OutputZeroValues_z(i) = OutputData(i).Fit_Torque_z.p2;
end

MeanFrictionSlope_x = mean(FrictionSlopeValues_x);
MeanFrictionSlope_y = mean(FrictionSlopeValues_y);
MeanFrictionSlope_z = mean(FrictionSlopeValues_z);

MaxFrictionSlope_x = max(FrictionSlopeValues_x);
MaxFrictionSlope_y = max(FrictionSlopeValues_y);
MaxFrictionSlope_z = max(FrictionSlopeValues_z);

MinFrictionSlope_x = min(FrictionSlopeValues_x);
MinFrictionSlope_y = min(FrictionSlopeValues_y);
MinFrictionSlope_z = min(FrictionSlopeValues_y);

MeanOutputSlope_x = mean(OutputSlopeValues_x);
MeanOutputSlope_y = mean(OutputSlopeValues_y);
MeanOutputSlope_z = mean(OutputSlopeValues_z);

MaxOutputSlope_x = max(OutputSlopeValues_x);
MaxOutputSlope_y = max(OutputSlopeValues_y);
MaxOutputSlope_z = max(OutputSlopeValues_z);

MinOutputSlope_x = min(OutputSlopeValues_x);
MinOutputSlope_y = min(OutputSlopeValues_y);
MinOutputSlope_z = min(OutputSlopeValues_z);

MeanFrictionZero_x = mean(FrictionZeroValues_x);
MeanFrictionZero_y = mean(FrictionZeroValues_y);
MeanFrictionZero_z = mean(FrictionZeroValues_z);

MaxFrictionZero_x = max(FrictionZeroValues_x);
MaxFrictionZero_y = max(FrictionZeroValues_y);
MaxFrictionZero_z = max(FrictionZeroValues_z);

MinFrictionZero_x = min(FrictionZeroValues_x);
MinFrictionZero_y = min(FrictionZeroValues_y);
MinFrictionZero_z = min(FrictionZeroValues_z);

MeanOutputZero_x = mean(OutputZeroValues_x);
MeanOutputZero_y = mean(OutputZeroValues_y);
MeanOutputZero_z = mean(OutputZeroValues_y);

MaxOutputZero_x = mean(OutputZeroValues_x);
MaxOutputZero_y = mean(OutputZeroValues_y);
MaxOutputZero_z = mean(OutputZeroValues_z);

MinOutputZero_x = mean(OutputZeroValues_x);
MinOutputZero_y = mean(OutputZeroValues_y);
MinOutputZero_z = mean(OutputZeroValues_z);

MeanSlopeSum_x = MeanOutputSlope_x - MeanFrictionSlope_x;
MeanSlopeSum_y = MeanOutputSlope_y - MeanFrictionSlope_y;
MeanSlopeSum_z = MeanOutputSlope_z - MeanFrictionSlope_z;

MeanZeroSum_x = MeanOutputZero_x - MeanFrictionZero_x;
MeanZeroSum_y = MeanOutputZero_y - MeanFrictionZero_y;
MeanZeroSum_z = MeanOutputZero_z - MeanFrictionZero_z;

SampleRWS_x = 0:1:160;
SampleRWS_y = 0:1:140;
SampleRWS_z = 0:1:100;

TorqueSum_x = MeanZeroSum_x + MeanSlopeSum_x*SampleRWS_x; 
TorqueSum_y = MeanZeroSum_y + MeanSlopeSum_y*SampleRWS_y; 
TorqueSum_z = MeanZeroSum_z + MeanSlopeSum_z*SampleRWS_z; 

OutputTorque_x = MeanOutputZero_x + MeanOutputSlope_x*SampleRWS_x;
OutputTorque_y = MeanOutputZero_y + MeanOutputSlope_y*SampleRWS_y;
OutputTorque_z = MeanOutputZero_z + MeanOutputSlope_z*SampleRWS_z;

figure(1)
subplot(3,1,1)
plot(SampleRWS_x,OutputTorque_x,SampleRWS_x,TorqueSum_x)
title({'Reaction wheel torque outputs (blue) and net "output+friction" torque (green)'; 'plotted against reaction wheel speeds'})
ylabel('X reaction wheel torque (Nm)')
subplot(3,1,2)
plot(SampleRWS_y,OutputTorque_y,SampleRWS_y,TorqueSum_y)
ylabel('Y reaction wheel torque (Nm)')
subplot(3,1,3)
plot(SampleRWS_z,OutputTorque_z,SampleRWS_z,TorqueSum_z)
ylabel('Z reaction wheel torque (Nm)')
xlabel('Reaction wheel speed (rad/s)')