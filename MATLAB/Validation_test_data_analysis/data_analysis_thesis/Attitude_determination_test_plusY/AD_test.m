clear all
load 'AD_data.mat'

AngleStep = 20/180*3.1415;
Breakpoints = [1 19 37];

for i = 1:(length(Breakpoints)-1)

ADtestResults(i).TrueHeading = zeros(1,(Breakpoints(i+1)-Breakpoints(i)));
ADtestResults(i).HeadingError = zeros(1,(Breakpoints(i+1)-Breakpoints(i)));
ADtestResults(i).MeasuredHeading = zeros(1,(Breakpoints(i+1)-Breakpoints(i)));
ADtestResults(i).MeasuredHeading(1) = mean(TestData(1).Heading);
ADtestResults(i).TrueHeading(1) = ADtestResults(i).MeasuredHeading(1);

Transition = 1;
for j = 2:(Breakpoints(i+1)-Breakpoints(i))
    ADtestResults(i).TrueHeading(j) = ADtestResults(i).TrueHeading(j-1)+AngleStep;
    if ADtestResults(i).TrueHeading(j) > 3.141592
        ADtestResults(i).TrueHeading(j) = ADtestResults(i).TrueHeading(j)-6.2832;
        Transition = j;
    end
    ADtestResults(i).MeasuredHeading(j) = mean(TestData(j+Breakpoints(i)-1).Heading);
    ADtestResults(i).HeadingError(j) = ADtestResults(i).MeasuredHeading(j) - ADtestResults(i).TrueHeading(j);
    if abs(ADtestResults(i).HeadingError(j)) > 3.141592 
    if sign(ADtestResults(i).HeadingError(j)) > 0
        ADtestResults(i).HeadingError(j) = ADtestResults(i).HeadingError(j)-6.2832;
    else
        ADtestResults(i).HeadingError(j) = ADtestResults(i).HeadingError(j)+6.2832;
    end
    end
end

if Transition == 1
    BeforeTransition = 18;
else
    BeforeTransition = Transition-1;
end

    MiddleValue = ADtestResults(i).HeadingError(BeforeTransition)+(ADtestResults(i).HeadingError(Transition) - ADtestResults(i).HeadingError(BeforeTransition)) / 2 ;
    ADtestResults(i).HeadingError((Breakpoints(i+1)-Breakpoints(i)+1): (Breakpoints(i+1)-Breakpoints(i)+2)) = MiddleValue;
    ADtestResults(i).TrueHeading(Breakpoints(i+1)-Breakpoints(i)+1) = 3.142;
    ADtestResults(i).TrueHeading(Breakpoints(i+1)-Breakpoints(i)+2)= -3.142;
    ADtestResults(i).MeasuredHeading(Breakpoints(i+1)-Breakpoints(i)+1) = 3.142;
    ADtestResults(i).MeasuredHeading(Breakpoints(i+1)-Breakpoints(i)+2)= -3.142;
end


figure(1)
plot(ADtestResults(1).TrueHeading(1:length(ADtestResults(1).TrueHeading)-2),ADtestResults(1).HeadingError(1:length(ADtestResults(1).TrueHeading)-2),'o',...
    ADtestResults(2).TrueHeading(1:length(ADtestResults(1).TrueHeading)-2), ADtestResults(2).HeadingError(1:length(ADtestResults(1).TrueHeading)-2),'x')
title('Heading error as a function of heading')
ylabel('Heading error (rad)')
xlabel('Heading as determined by the protractor')
figure(2)
plot(ADtestResults(1).TrueHeading(1:length(ADtestResults(1).TrueHeading)-2),'r')
hold on
plot(ADtestResults(1).MeasuredHeading(1:length(ADtestResults(1).TrueHeading)-2),'o')
plot(ADtestResults(2).MeasuredHeading(1:length(ADtestResults(1).TrueHeading)-2),'xm')
hold off
title('Measured heading values (blue and purple) plotted against true heading (red)')
xlabel('Data point number')
ylabel('Heading (rad)')

save('AD_results.mat', 'ADtestResults')