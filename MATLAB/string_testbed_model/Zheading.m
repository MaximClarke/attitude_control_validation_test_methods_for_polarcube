% Zheading.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% Zheading.m calculates heading of the body x axis with respect to magnetic 
% north provided readings from a magnetometer and
% accelerometer. Yheading only works if the z body axis is pointed above
% the horizontal

function [Heading] = Zheading(MagFieldVector, GravityVector)
% Calculate magnetic west
DueWest = cross(MagFieldVector, GravityVector);
DueWest = DueWest/norm(DueWest);

% Cross product of the x body axis and gravity
XcrossGrav = cross([1 0 0], GravityVector);
XcrossGrav = XcrossGrav/norm(XcrossGrav);

% Calculate heading
Heading = acos(dot(DueWest, XcrossGrav));
% Positive or neagitve angle (This is the reason you need 3 heading
% formulas)
if DueWest(1) < 0
    Heading = -Heading;
end

end