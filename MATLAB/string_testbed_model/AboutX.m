% AboutX.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com
% AboutX is a function that produces a direction-cosine matrix
% corersponding to a rotation of "Heading" radians about the Body X axis.
% This function was designed to be used by the AttitudeUncertaintyModel.m
% script because it assumes that rotations will only occur about one of the
% primary body axes.

function [DCM] = AboutX(Heading)
DCM = [1 0 0; 0 cos(Heading) -sin(Heading); 0 sin(Heading) cos(Heading)];

end