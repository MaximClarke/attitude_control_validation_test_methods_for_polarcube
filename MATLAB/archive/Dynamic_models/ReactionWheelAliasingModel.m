% ReactionWheelAliasingModel.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% ReactionWheelAliasingModel uses a monte-carlo type approach to determine
% the uncertainty in magnetometer measurement that is introduced by  
% sinusoidal disturbances produced by the rotating permagnets in the reaction
% wheel motors. 

% A disturbing signal composed of 3 sinusoids of equal amplitude is sampled 
% at specific frequency over a period and averaged. 
% The mean value of the samples will have an error. Uncertainty
% is characterized by performing this several times with slight variation
% in sample times and sinusoid phase. 

% This script produces a sample set that represents uncertainty as a
% function of the speed of a single reaction wheel for a fixed sample frequency 
% and fixed speed of the secondary reaction wheels.


%% Set parameters
SampleFrequency = 160; %Hz
MeasurementPeriod = 0.7; %s
MeasurementNumber = 100; 
MaxReactionWheelSpeed = 999; %rad/s
SecondaryReactionWheelSpeed = 150; %rad/s

SampleTimeUncertainty = (1/SampleFrequency/30); %std of sample time error


SampleTimes = 1:(SampleFrequency*MeasurementPeriod); 
SampleTimes = SampleTimes/SampleFrequency; %s

% empty vectors
MeasurementError = zeros(1,MeasurementNumber);
ErrorRMS = zeros(1,(MaxReactionWheelSpeed+1));

%% Calculation

% Determine uncertainty for the full set of reaction wheel speeds.
for ReactionWheelSpeed = 0:1:MaxReactionWheelSpeed
    
    % Perform several measurements
    for Measurement = 1:1:MeasurementNumber
    % Add noise to sample times    
    NoiseySampleTimes = SampleTimes + randn(1,length(SampleTimes))*SampleTimeUncertainty;
    % Sample a signal comprised of 3 sinusoids with unknown phase and a mean value
    % of 0
    Samples = sin(rand + NoiseySampleTimes*ReactionWheelSpeed)+...
        sin(rand + NoiseySampleTimes*SecondaryReactionWheelSpeed)+...
        sin(rand + NoiseySampleTimes*SecondaryReactionWheelSpeed);
    % Calculate and log measurement error
    MeasurementError(Measurement) = mean(Samples);
    end
    % Calculate and log measurement uncerainty
    ErrorRMS(ReactionWheelSpeed+1) = rms(MeasurementError);
end

% Plot results
figure(1)
plot(0:1:MaxReactionWheelSpeed,ErrorRMS)
title('Magnetometer measurement uncertainty as a function of reaction wheel speed')
xlabel('Reaction wheel speed (rad/s)')
ylabel('std of magnetometer measurement error (1/disturbance amplitude)')