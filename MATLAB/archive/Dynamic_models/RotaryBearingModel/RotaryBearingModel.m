% RotaryBearingModel.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% RotaryBearingModel.m simulates the dynamics of a test model mounted on a
% rotary bearing testbed the model is subject to air drag, gravity and
% gyroscopic torques of the reaction wheels. It is also constrained to
% motion about the rotation axis of the bearing. All torques that are not
% parallel to the rotation axis will not contribute to motion.

%% Set parameters

% Timestamp
t = 10;
dt = 0.01;
time = dt:dt:t;
outputlength = t/dt;

% Inertial gravity field vector
g= [0; 0; -9.81];

% Drag coefficient
CD = 0.5;

% Moment of inertia of the reaction wheel
mRW = 0.0265;
rRW = 0.025;
JRW=0.5*mRW*rRW^2;

% Moment of inertia of the test model
mb = 1.4; % kg
hb = 0.10;
wb = 0.10;
db = 0.33;
IB=[mb*(hb^2+db^2)/12 0 0;
    0 mb*(wb^2+db^2)/12 0;
    0 0 mb*(hb^2+wb^2)/12];

% Alignment error of the center of mass of the test model with respect to
% the rotation axis in the body frame
ecm = [0.05; 0.00; 0.001];

% Agnular alignmet error of the bearing with respect to the gravity vector
% Error rotation about X
TBerrorX = 0.0;
% Error rotaiton about Y
TBerrorY = 0.01;

% DCM of the relative orientation of the testbed (T) to Earth (E)
RET=[cos(TBerrorY) sin(TBerrorX)*sin(TBerrorY) cos(TBerrorX)*sin(TBerrorY);
    0 cos(TBerrorX) -sin(TBerrorX);
    -sin(TBerrorY) sin(TBerrorX)*cos(TBerrorY) cos(TBerrorX)*cos(TBerrorY)];

% DCM of the relative orientation of initial position of the test model
% body (Bo) to the testbed (T)
RTBo=[0 1 0; 0 0 1; 1 0 0];

% Empty vectors
omegaBB = zeros(3,outputlength); % Roation rate of the test model body with respect to its own frame
omegaBBdot = zeros(3,outputlength); % Rate change in rotation rate
omegaBoB = zeros(3,outputlength); % Roation rate of the test model body with respect to its intial position
omegaRW = zeros(3,outputlength); % Reaction wheel rates
RBoB = zeros(3,3,outputlength);
REB = zeros(3,3,outputlength);
Tgyro = zeros(3,outputlength);
Tgrav = zeros(3,outputlength);
TD = zeros(3,outputlength);
TLC = zeros(3,outputlength);
Tilt = zeros(2,outputlength);

% initial conditions
% Set an initial angular velocity to observe motion
omegaBB(:,1) = [1;0;0];
RBoB(:,:,1) = [1 0 0; 0 1 0; 0 0 1];
omegaBoB(:,1) = RBoB(:,:,1)*omegaBB(:,1);
REB(:,:,1) = RET*RTBo*RBoB(:,:,1);

%% Calculate

for i=2:length(time)
    % Integrate the position of the test model
    RBoB(:,:,i)= RBoB(:,:,i-1)+ dt*(CrossMatrix(omegaBoB(:,i-1))*RBoB(:,:,(i-1)));
    % orthonormalize the DCM
    RBoB(:,:,i)= GSNormalize(RBoB(:,:,i),1,2,3);
    % DCM of the orientation of the test model with respect to Earth
    REB(:,:,i)= RET*RTBo*RBoB(:,:,i);
    % integrate the roation rate of the test model
    omegaBB(:,i) = omegaBB(:,i-1)+dt*omegaBBdot(:,i-1);
    % calculate rotation rate in Bo frame
    omegaBoB(:,i) = RBoB(:,:,i)*omegaBB(:,i);
    % gyroscopic torques
     Tgyro(:,i)=-CrossMatrix(omegaBB(:,i))*(JRW*omegaRW(:,i));
     % gravitational torques
     Tgrav(:,i)= CrossMatrix(REB(:,:,i)'*(g*mb))*ecm;
     % Drag torques
     TD(:,i) = -CD*omegaBB(:,i).^2;
     % Equation of motion
     % All torques that are not parallel to the rotation axis are removed
    omegaBBdot(:,i) = IB\([Tgyro(1,i); 0; 0] + [Tgrav(1,i); 0; 0] + [TD(1,i); 0; 0]);
     TLC(:,i)=RTBo*RBoB(:,:,i)*(Tgyro(:,i) + Tgrav(:,i) + TD(:,i)+CrossMatrix(omegaBB(:,i))*(IB*omegaBB(:,i)));
    
end

%% Plot
figure(1)
plot(time,TLC(1,:),time,TLC(2,:));
title('Graviational torque output to the load cells ')
xlabel('Time (s)')
ylabel('Torque (Nm)')

figure(2)
subplot(2,1,1)
plot(time,Tgrav(1,:))
title('Graviational torque on the test model body about the rotation axis')
xlabel('Time (s)')
ylabel('Torque (Nm)')
subplot(2,1,2)
plot(time,TD(1,:))
title('Air drag torque on the test model body about the rotation axis')
xlabel('Time (s)')
ylabel('Torque (Nm)')

