
IB=[0.0122 -8.44e-5 -8.44e-5;
    -8.44e-5 0.0847 0.00308;
    -8.44e-5 0.00308 0.0848];
target_omegaDot = [0; 0; 0];
MovingAverageOmegaFromGyro = [0.1; 0.1; 0.1];
target_omega= [0; 0; 0];
K = 0.003;
P = [0.0075 0 0; 0 0.0075 0; 0 0 0.0075];
Ki = [0.005 0 0; 0 0.005 0; 0 0 0.005];
J_RW = 1.12e-5;
omegaRW = [50; 50; 50];
sigInt = [0; 0; 0];
MeasuredMRP = [0.1; 0.1; 0.1];
TargetMRP = [0; 0; 0];
    ErrorMRP = diff_sigma(MeasuredMRP,TargetMRP); % MRP of attitude error
    d_omega = MovingAverageOmegaFromGyro - target_omega; % error in body rate
    

    zeta = [0.1; 0.1; 0.1];

part1 =     -IB*(target_omegaDot - CrossMatrix(MovingAverageOmegaFromGyro)*target_omega);
sum2 =      K*ErrorMRP + P*d_omega + P*Ki*zeta;
sum3 = (CrossMatrix(target_omega)-CrossMatrix(Ki*zeta))*(IB*MovingAverageOmegaFromGyro + J_RW*(omegaRW+MovingAverageOmegaFromGyro));
sum31 = (CrossMatrix(target_omega)-CrossMatrix(Ki*zeta));
sum32 = (IB*MovingAverageOmegaFromGyro + J_RW*(omegaRW+MovingAverageOmegaFromGyro));

ControlTorque = -IB*(target_omegaDot - CrossMatrix(MovingAverageOmegaFromGyro)*target_omega) + K*ErrorMRP + P*d_omega + P*Ki*zeta...
        - (CrossMatrix(target_omega)-CrossMatrix(Ki*zeta))*(IB*MovingAverageOmegaFromGyro + J_RW*(omegaRW+MovingAverageOmegaFromGyro));
   