% omega2sigmaD.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% TestbedEOM contains the equations of motion of the test model on the
% string testbed according to rigid body dynammics. Any torques that are
% not parallel to the rotation axis are removed, analogously to how they
% would be removed by gravity.
function [omega_dot, sigma_dot] = MRP_EOM(omega,sigma,Omega,torques,I,J_w)
% Angular momentum of the reaction wheels
h_s = J_w * eye(3) * ( Omega + omega );
% Derive motion, remove perpendicular torques
omega_dot = I \ (- CrossMatrix(omega) * I * omega - CrossMatrix(omega) * h_s + torques);
% rate change of RMP for integration
sigma_dot = omega2sigmaD(omega,sigma);
