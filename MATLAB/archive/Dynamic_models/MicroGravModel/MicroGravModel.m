% StringTestbedModel.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% StringTestbedModel.m is a dynamic model of test model motion on the
% string suspension testbed. The test model is subject to spring toreqs and
% damping torques from the string as well as gyroscopic torques from the
% reaction wheels. The test model is slao constrined to motion about the
% local vertical (the rotation axis) torques that are not parallel to the
% roation axis do not affect test model motion.

%% Input parameters

t= 100; % (length of propagation)

BFRotationAxis = [1; 0; 0]; % Axis about which to perform a slew maneuver
RotationAngle = 1.5; % rad

SampleBinPeriod = 0.2; % seconds
SampleFrequency  = 160; % Hertz

ControlPeriod = 0.1; % seconds
ControlStepSize = ControlPeriod*SampleFrequency;
ControlCounter = round(rand*10);
ControlTorque  = [0; 0; 0];


DeterminationPeriod = 0.05;
DeterminationStepSize = DeterminationPeriod*SampleFrequency;
DeterminationCounter = round(rand*10);


%Initial test model conditions
InitialRWSpeeds = [50+(rand*5-2.5); 50+(rand*5-2.5); 50+(rand*5-2.5)]; % Initial Speeds of the reaction wheels
InitialBodyRate = 0; % initial rotation rate about the rotation axis

% Control law Gains
% With Attitude control
K = 0.0075; % Potential gains
P = diag([0.05; 0.05; 0.05]); % Derivative gains
Ki = diag([0; 0; 0]);% Integral gains


%Gyro Properties
GyroSensitivity = 8.75; %mdps/count
Gyro1SensitivityError = 0; %mdps/count
Gyro2SensitivityError = 0; %mdps/count
Gyro1ZeroRateLevel = [ 0; 0; 0]; %counts
Gyro2ZeroRateLevel = [ 0; 0; 0]; %counts
Gyro1NoiseRMS = [30; 30; 30]; %counts
Gyro2NoiseRMS = [30; 30; 30]; %counts

% Moment of inertia of the reaction wheels
J_RW=1.12e-5;

% Moment of inertia of the Test model body (from CAD)

IB=[0.002106 -1.1e-5 -0.00012;
    -1.1e-5 0.00214 1.58e-5;
    -0.00012 1.58e-5 0.000975];

%% Setup

% Normalize the roation axis in the body frame
BFRotationAxis = BFRotationAxis/norm(BFRotationAxis);
TargetMRP = BFRotationAxis * tan(RotationAngle/4);

% Sensor Sampling Properties
SamplePeriod = 1/SampleFrequency; % seconds
SamplesPerMeasurement = SampleFrequency*SampleBinPeriod; % # of samples per measurement
SampleCount = 0;

% timestamp
dt = 1/SampleFrequency; %(timestep at the sample frequency of the magnetometer)
simtime = dt:dt:t; % Timestamp of the simulation
outputlength = t/dt; % useful for defining empty vectors

% Empty vectors
% Test model body motion
omegaBB = zeros(3,outputlength); % Body rotation rates in the body frame
omegaRW = zeros(3,outputlength); % Reaction wheel speeds
MRP = zeros(3,outputlength);     % Attitude in Modified Rodriguez parameters
ErrorMRP = zeros(3,outputlength);% MRP representing attitude error
torques = zeros(3,outputlength);% MRP representing attitude error
MeasuredMRP = zeros(3,outputlength);
omegaBBfromGyro = zeros(3,outputlength);
MovingAverageOmegaFromGyro = [0; 0; 0];
MeasuredMRP_TV = [0; 0; 0];

omegaRW(:,1) = InitialRWSpeeds; % Reaction wheel speeds

% torques
Tcontrol = zeros(3,outputlength);

% initial conditions

% Control Law parameters
sigInt = [0; 0; 0];
target_omega = [0; 0; 0];
target_omegaDot = [0; 0; 0];
d_omega0 = omegaBB(:,1) - target_omega;

% Wait bar indicates progress
progressbar;

%% Compute

for i=2:length(simtime)
    
    progressbar(i/length(simtime))
%% Propagate Dynamics
    % Rigid body dynamics
    [omegaBB(:,i),MRP(:,i)] = RK4integrate(dt,omegaBB(:,i-1),MRP(:,i-1),omegaRW(:,i-1),torques(:,i-1),IB,J_RW);
    
    % Integrate reaction wheel speed
    omegaRW(:,i) = omegaRW(:,i-1)+dt/J_RW*Tcontrol(:,i-1);
    
%% Determine attitude
    
    % Gyro output
    % Simulate output
     Gyro1Out = omegaBB(:,i)/3.141592*180*1000/(GyroSensitivity+Gyro1SensitivityError)+ Gyro1ZeroRateLevel + Gyro1NoiseRMS.*randn(3,1);
     Gyro2Out = omegaBB(:,i)/3.141592*180*1000/(GyroSensitivity+Gyro2SensitivityError)+ Gyro2ZeroRateLevel + Gyro2NoiseRMS.*randn(3,1);
     
     MeanGyroOut = (Gyro1Out+Gyro2Out)/2;
    
     % Calculate rate from output
     omegaBBfromGyro(:,i) = MeanGyroOut/1000/180*3.141592*GyroSensitivity;
    
    % Predictor-corrector calculation of theta
    % If first direct attitude measurement has yet to arrive, propagate
    % gyro

    if DeterminationCounter > DeterminationStepSize
        if i > DeterminationCounter
        MovingAverageOmegaFromGyro = omegaBBfromGyro(:,i);% mean(omegaBBfromGyro(:,(i-DeterminationCounter):i),2);
        end
        MeasuredMRP_TV = MeasuredMRP_TV + dt*DeterminationCounter*omega2sigmaD(MovingAverageOmegaFromGyro, MeasuredMRP_TV);
       % [ExpectedomegaBB,MeasuredMRP_TV] = RK4integrate(dt*DeterminationCounter,MovingAverageOmegaFromGyro,MeasuredMRP_TV,omegaRW(:,i-1),torques(:,i-1),IB,J_RW);
        DeterminationCounter = 0;
    end
  MeasuredMRP(:,i) = MeasuredMRP_TV; 
    DeterminationCounter = DeterminationCounter + 1 ;
    
%% Attitude control
    
    % Error terms
    ErrorMRP(:,i) = diff_sigma(MeasuredMRP(:,i),TargetMRP); % MRP of attitude error
    d_omega = MovingAverageOmegaFromGyro - target_omega; % error in body rate
    
    % Error integrator
    sigInt = sigInt + ErrorMRP(:,i)*dt;
    zeta = K*sigInt + IB*(d_omega - d_omega0);
       
    % Control law (Motor output torque)(Nm) Hogan Schaub 2013
    if isnan(ErrorMRP(:,i))
    elseif ControlCounter > ControlStepSize
    ControlTorque = -IB*(target_omegaDot - CrossMatrix(MovingAverageOmegaFromGyro)*target_omega) + K*ErrorMRP(:,i) + P*d_omega + P*Ki*zeta...
        - (CrossMatrix(target_omega)-CrossMatrix(Ki*zeta))*(IB*MovingAverageOmegaFromGyro + J_RW*(omegaRW(:,i)+MovingAverageOmegaFromGyro));
    ControlCounter = 0;
    end
    
    for j = 1:3
    if abs(ControlTorque(j)) > 0.00025
        ControlTorque(j) = 0.00025*sign(ControlTorque(j));
    end
    end
    
    
    ControlCounter = ControlCounter + 1;
    
    Tcontrol(:,i) = ControlTorque;
    
%% Testbed dynamics
    
    % Sum of torques     
    torques(:,i) = -Tcontrol(:,i);
end

%% Plot

figure(1)
subplot(3,1,1)
plot(simtime,Tcontrol(1,:))
title('Motor output torques')
ylabel('x (Nm)')
subplot(3,1,2)
plot(simtime,Tcontrol(2,:))
ylabel('y (Nm)')
subplot(3,1,3)
plot(simtime,Tcontrol(3,:))
ylabel('z (Nm)')
xlabel('Time (s)')

figure(2)
subplot(3,1,1)
plot(simtime,omegaRW(1,:))
title('reaction wheel speeds')
ylabel('x (rad/s)')
subplot(3,1,2)
plot(simtime,omegaRW(2,:))
ylabel('y (rad/s)')
subplot(3,1,3)
plot(simtime,omegaRW(3,:))
ylabel('z (rad/s)')
xlabel('Time (s)')

targetMRPs = ones(3,length(simtime));
targetMRPs(1,:) = targetMRPs(1,:) * TargetMRP(1);
targetMRPs(2,:) = targetMRPs(2,:) * TargetMRP(2);
targetMRPs(3,:) = targetMRPs(3,:) * TargetMRP(3);


figure(3)
plot(simtime, ErrorMRP(1,:), simtime, ErrorMRP(2,:), simtime, ErrorMRP(3,:))
title('Measured attitude error from gyroscopic propagation')
ylabel('Error MRP')
xlabel('Time (s)')
legend('x', 'y', 'z')

figure(4)
subplot(3,1,1)
plot(simtime,MRP(1,:),simtime,MeasuredMRP(1,:),simtime,targetMRPs(1,:))
title('Actual motion (blue) compared to measured motion (green)')
ylabel('x MRP')
subplot(3,1,2)
plot(simtime,MRP(2,:),simtime,MeasuredMRP(2,:),simtime,targetMRPs(2,:))
ylabel('y MRP')
subplot(3,1,3)
plot(simtime,MRP(3,:),simtime,MeasuredMRP(3,:),simtime,targetMRPs(3,:))
ylabel('z MRP)')
xlabel('Time')

ErrorMRPmagnitude = (ErrorMRP(1,:).^2+ErrorMRP(2,:).^2+ErrorMRP(3,:).^2).^0.5;
TotalMeasuredAngularError = atan(ErrorMRPmagnitude)*4;
figure(5)
plot(simtime, TotalMeasuredAngularError)
title('Total measured angular error')
ylabel('Angular error (rad)')
xlabel('Time')



