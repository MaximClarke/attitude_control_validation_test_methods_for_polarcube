% MRPfromTRIAD.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% MRPfromTRIAD calculates atttitude in terms of a Modified Rodriguez
% Parameter from two reference vectors via a TRAID algorithm.
function [sigma] = MRPfromTRIAD(MagneticFieldVector,GravityFieldVector)

    % Calculate the West vector by crossing the magnetic field vector with
    % the gravity field vector
    dueWEST = cross(MagneticFieldVector,GravityFieldVector);
    % Normalize
    dueWEST = dueWEST/norm(dueWEST);
    GravityFieldVector = GravityFieldVector/norm(GravityFieldVector);
    % Calculate the North vector by crossing the normalized magnetic field
    % vector witht the normalized gravity field vector.
    dueNORTH = cross(GravityFieldVector,dueWEST);
    % DCM of attitude (Earth X is North, Y is West and Z is up)
    dcmEB = [dueNORTH dueWEST -GravityFieldVector];
    dcmEB = GSNormalize(dcmEB,3,2,1);
    % convert DCM to MRP
    sigma = SheppardMRP(dcmEB);
end