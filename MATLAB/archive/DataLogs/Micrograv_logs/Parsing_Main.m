clear all

files = dir('Raw_data_files');
IdentificationColumn  = 32;
TestNumber = 0;

TestData.Description = [];
for i = 3:length(files)
    data = load(strcat('.\Raw_data_files\',  files(i).name));
    j = 2;
    while j < length(data(:,1))
        if data(j,IdentificationColumn) ~= 0
            j = j+1;
        else
            if data(j-1,IdentificationColumn) ~= 0 || j == 2
                TestNumber = TestNumber + 1;
                TestIndex = 1;
            end
    TestData(TestNumber).gyro1_x(TestIndex) = data(j,1);
    TestData(TestNumber).gyro1_y(TestIndex) = data(j,2);
    TestData(TestNumber).gyro1_z(TestIndex) = data(j,3);
    TestData(TestNumber).gyro2_x(TestIndex) = data(j,4);
    TestData(TestNumber).gyro2_y(TestIndex) = data(j,5);
    TestData(TestNumber).gyro2_z(TestIndex) = data(j,6);
    TestData(TestNumber).RWS_x(TestIndex) = data(j,7);
    TestData(TestNumber).RWS_y(TestIndex) = data(j,8);
    TestData(TestNumber).RWS_z(TestIndex) = data(j,9);
    TestData(TestNumber).K(TestIndex) = data(j,10);
    TestData(TestNumber).Ki_x(TestIndex) = data(j,11);
    TestData(TestNumber).Ki_y(TestIndex) = data(j,12);
    TestData(TestNumber).Ki_z(TestIndex) = data(j,13);
    TestData(TestNumber).P_x(TestIndex) = data(j,14);
    TestData(TestNumber).P_y(TestIndex) = data(j,15);
    TestData(TestNumber).P_z(TestIndex) = data(j,16);
    TestData(TestNumber).Target_MRP_x(TestIndex) = data(j,17);
    TestData(TestNumber).Target_MRP_y(TestIndex) = data(j,18);
    TestData(TestNumber).Target_MRP_z(TestIndex) = data(j,19);
    TestData(TestNumber).Target_rate_x(TestIndex) = data(j,20);
    TestData(TestNumber).Target_rate_y(TestIndex) = data(j,21);
    TestData(TestNumber).Target_rate_z(TestIndex) = data(j,22);
    TestData(TestNumber).Target_RWS_x(TestIndex) = data(j,23);
    TestData(TestNumber).Target_RWS_y(TestIndex) = data(j,24);
    TestData(TestNumber).Target_RWS_z(TestIndex) = data(j,25);
    TestData(TestNumber).gyro1_bias_x(TestIndex) = data(j,26);
    TestData(TestNumber).gyro1_bias_y(TestIndex) = data(j,27);
    TestData(TestNumber).gyro1_bias_z(TestIndex) = data(j,28);
    TestData(TestNumber).gyro2_bias_x(TestIndex) = data(j,29);
    TestData(TestNumber).gyro2_bias_y(TestIndex) = data(j,30);
    TestData(TestNumber).gyro2_bias_z(TestIndex) = data(j,31);
    TestData(TestNumber).Control_mode(TestIndex) = data(j,32);
    TestData(TestNumber).MRP_x(TestIndex) = data(j,33);
    TestData(TestNumber).MRP_y(TestIndex) = data(j,34);
    TestData(TestNumber).MRP_z(TestIndex) = data(j,35);
    TestData(TestNumber).Error_MRP_x(TestIndex) = data(j,36);
    TestData(TestNumber).Error_MRP_y(TestIndex) = data(j,37);
    TestData(TestNumber).Error_MRP_z(TestIndex) = data(j,38); 
            
    TestIndex = TestIndex+1;
    j = j + 1;
        end
                
    end
end
TestsToDelete = [4, 9, 12];

for i = 1:length(TestsToDelete)
    TestData(TestsToDelete(i)-(i-1))= [];
end

TruncationMatrix = [0, 0;
                    0, 0;
                    0, 0;
                    0, 0;
                    0, 0;
                    0, 0;
                    0, 0;
                    0, 0;
                    0, 0;
                    0, 0];

run('TruncateTests')
run('NameTheTests')

clear i j IdentificationColumn TruncationMatrix TestsToDelete TestIndex TestNumber files data TestDescriptions
save 'Parsed_Micrograv.mat'
