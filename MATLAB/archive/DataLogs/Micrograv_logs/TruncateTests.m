

for i = 1:length(TruncationMatrix(:,1))
    
    if TruncationMatrix(i,1) == 0
        LowerBound = 1;
    else 
        LowerBound = TruncationMatrix(i,1);
    end
    
    if TruncationMatrix(i,2) == 0
        UpperBound = length(TestData(i).gyro1_x);
    else
        UpperBound = TruncationMatrix(i,2);
    end
        
    TestData(i).gyro1_x = TestData(i).gyro1_x(LowerBound:UpperBound);
    TestData(i).gyro1_y = TestData(i).gyro1_y(LowerBound:UpperBound);
    TestData(i).gyro1_z = TestData(i).gyro1_z(LowerBound:UpperBound);
    TestData(i).gyro2_x = TestData(i).gyro2_x(LowerBound:UpperBound);
    TestData(i).gyro2_y = TestData(i).gyro2_y(LowerBound:UpperBound);
    TestData(i).gyro2_z = TestData(i).gyro2_z(LowerBound:UpperBound);
    TestData(i).RWS_x = TestData(i).RWS_x(LowerBound:UpperBound);
    TestData(i).RWS_y = TestData(i).RWS_y(LowerBound:UpperBound);
    TestData(i).RWS_z = TestData(i).RWS_z(LowerBound:UpperBound);
    TestData(i).K = TestData(i).K(LowerBound:UpperBound);
    TestData(i).Ki_x = TestData(i).Ki_x(LowerBound:UpperBound);
    TestData(i).Ki_y = TestData(i).Ki_y(LowerBound:UpperBound);
    TestData(i).Ki_z = TestData(i).Ki_z(LowerBound:UpperBound);
    TestData(i).P_x = TestData(i).P_x(LowerBound:UpperBound);
    TestData(i).P_y = TestData(i).P_y(LowerBound:UpperBound);
    TestData(i).P_z = TestData(i).P_z(LowerBound:UpperBound);
    TestData(i).Target_MRP_x = TestData(i).Target_MRP_x(LowerBound:UpperBound);
    TestData(i).Target_MRP_y = TestData(i).Target_MRP_y(LowerBound:UpperBound);
    TestData(i).Target_MRP_z = TestData(i).Target_MRP_z(LowerBound:UpperBound);
    TestData(i).Target_rate_x = TestData(i).Target_rate_x(LowerBound:UpperBound);
    TestData(i).Target_rate_y = TestData(i).Target_rate_y(LowerBound:UpperBound);
    TestData(i).Target_rate_z = TestData(i).Target_rate_z(LowerBound:UpperBound);
    TestData(i).Target_RWS_x = TestData(i).Target_RWS_x(LowerBound:UpperBound);
    TestData(i).Target_RWS_y = TestData(i).Target_RWS_y(LowerBound:UpperBound);
    TestData(i).Target_RWS_z = TestData(i).Target_RWS_z(LowerBound:UpperBound);
    TestData(i).gyro1_bias_x = TestData(i).gyro1_bias_x(LowerBound:UpperBound);
    TestData(i).gyro1_bias_y = TestData(i).gyro1_bias_y(LowerBound:UpperBound);
    TestData(i).gyro1_bias_z = TestData(i).gyro1_bias_z(LowerBound:UpperBound);
    TestData(i).gyro2_bias_x = TestData(i).gyro2_bias_x(LowerBound:UpperBound);
    TestData(i).gyro2_bias_y = TestData(i).gyro2_bias_y(LowerBound:UpperBound);
    TestData(i).gyro2_bias_z = TestData(i).gyro2_bias_z(LowerBound:UpperBound);
    TestData(i).Control_mode = TestData(i).Control_mode(LowerBound:UpperBound);
    TestData(i).MRP_x = TestData(i).MRP_x(LowerBound:UpperBound);
    TestData(i).MRP_y = TestData(i).MRP_y(LowerBound:UpperBound);
    TestData(i).MRP_z = TestData(i).MRP_z(LowerBound:UpperBound);
    TestData(i).Error_MRP_x = TestData(i).Error_MRP_x(LowerBound:UpperBound);
    TestData(i).Error_MRP_y = TestData(i).Error_MRP_y(LowerBound:UpperBound);
    TestData(i).Error_MRP_z = TestData(i).Error_MRP_z(LowerBound:UpperBound); 
end

clear LowerBound UpperBound