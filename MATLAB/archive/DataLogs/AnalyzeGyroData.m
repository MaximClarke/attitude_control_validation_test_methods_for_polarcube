data=dlmread('GyroData/GyroDataSample1.txt');
XaxisFH = [mean(data(round(1:length(data(:,1))/2),2)) std(data(round(1:length(data(:,1))/2),2))];
YaxisFH = [mean(data(round(1:length(data(:,1))/2),3)) std(data(round(1:length(data(:,1))/2),3))];
ZaxisFH = [mean(data(round(1:length(data(:,1))/2),4)) std(data(round(1:length(data(:,1))/2),4))];

XaxisSH = [mean(data(round(length(data(:,1))/2:length(data(:,1))),2)) std(data(round(length(data(:,1))/2:length(data(:,1))),2))];
YaxisSH = [mean(data(round(length(data(:,1))/2:length(data(:,1))),3)) std(data(round(length(data(:,1))/2:length(data(:,1))),3))];
ZaxisSH = [mean(data(round(length(data(:,1))/2:length(data(:,1))),4)) std(data(round(length(data(:,1))/2:length(data(:,1))),4))];