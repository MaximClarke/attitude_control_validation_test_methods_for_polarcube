clear all
load 'Output_data.mat'


SampleTimestamp = 0:0.1:100;
    
for j = 1:length(TestData)
    FitData(j).RWS_x = fit(TestData(j).Timestamp', TestData(j).RWS_x','poly2');
    FitData(j).RWS_y = fit(TestData(j).Timestamp', TestData(j).RWS_y','poly2');
    for i = 1:length(SampleTimestamp)
    OutputData(j).Torque_x(i) = (FitData(j).RWS_x.p2 + 2*FitData(j).RWS_x.p1*SampleTimestamp(i))*1.12e-5;
    OutputData(j).RWS_x(i) = FitData(j).RWS_x.p3 + FitData(j).RWS_x.p2*SampleTimestamp(i)+ FitData(j).RWS_x.p1*SampleTimestamp(i)^2;
    
    OutputData(j).Torque_y(i) = (FitData(j).RWS_y.p2 + 2*FitData(j).RWS_y.p1*SampleTimestamp(i))*1.12e-5;
    OutputData(j).RWS_y(i) = FitData(j).RWS_y.p3 + FitData(j).RWS_y.p2*SampleTimestamp(i) + FitData(j).RWS_y.p1*SampleTimestamp(i)^2;
    end
    OutputData(j).Fit_Torque_x = fit(OutputData(j).RWS_x', OutputData(j).Torque_x','poly3');
    OutputData(j).Fit_Torque_y = fit(OutputData(j).RWS_y', OutputData(j).Torque_y','poly3');
end


figure(1)
subplot(2,1,1)
hold on
for i = 1:length(TestData)
plot(OutputData(i).Fit_Torque_x, OutputData(i).RWS_x, OutputData(i).Torque_x)
end
hold off
subplot(2,1,2)
hold on
for i = 1:length(TestData)
plot(OutputData(i).Fit_Torque_y, OutputData(i).RWS_y, OutputData(i).Torque_y)
end
hold on
figure(2)
subplot(2,1,1)
hold on
for i = 1:length(TestData)
plot(FitData(i).RWS_x, TestData(i).Timestamp, TestData(i).RWS_x)
end
hold off
subplot(2,1,2)
hold on
for i = 1:length(TestData)
plot(FitData(i).RWS_y, TestData(i).Timestamp, TestData(i).RWS_y)
end
hold off
figure(3)
subplot(2,1,1)
plot(TestData(1).Timestamp, TestData(1).RWS_x, TestData(2).Timestamp, TestData(2).RWS_x, TestData(3).Timestamp, TestData(3).RWS_x, TestData(4).Timestamp, TestData(4).RWS_x, TestData(5).Timestamp, TestData(5).RWS_x, TestData(6).Timestamp, TestData(6).RWS_x, TestData(7).Timestamp, TestData(7).RWS_x, TestData(8).Timestamp, TestData(8).RWS_x)
xlabel('Time (s)')
ylabel('Reaction wheel speed (rad/s)')
subplot(2,1,2)
plot(OutputData(1).RWS_x, OutputData(1).Torque_x, OutputData(2).RWS_x, OutputData(2).Torque_x, OutputData(3).RWS_x, OutputData(3).Torque_x, OutputData(4).RWS_x, OutputData(4).Torque_x, OutputData(5).RWS_x, OutputData(5).Torque_x, OutputData(6).RWS_x, OutputData(6).Torque_x, OutputData(7).RWS_x, OutputData(7).Torque_x, OutputData(8).RWS_x, OutputData(8).Torque_x)
xlabel('Reaction wheel speed (rad/s)')
ylabel('Output torque (rad/s)')

