clear all
load 'Friction_data.mat'


    meanp1_x = 0;
    meanp2_x = 0;
    meanp3_x = 0;
    
    meanp1_y = 0;
    meanp2_y = 0;
    meanp3_y = 0;
    
for j = 1:length(TestData)
    FitData(j).RWS_x = fit(TestData(j).Timestamp', TestData(j).RWS_x','poly2');
    meanp1_x = meanp1_x + FitData(j).RWS_x.p1;
    meanp2_x = meanp2_x + FitData(j).RWS_x.p2;
    meanp3_x = meanp3_x + FitData(j).RWS_x.p3;
    FitData(j).RWS_y = fit(TestData(j).Timestamp', TestData(j).RWS_y','poly2');
    meanp1_y = meanp1_y + FitData(j).RWS_y.p1;
    meanp2_y = meanp2_y + FitData(j).RWS_y.p2;
    meanp3_y = meanp3_y + FitData(j).RWS_y.p3;
end

    meanp1_x = meanp1_x/length(TestData);
    meanp2_x = meanp2_x/length(TestData);
    meanp3_x = meanp3_x/length(TestData);
    
    meanp1_y = meanp1_y/length(TestData);
    meanp2_y = meanp2_y/length(TestData);
    meanp3_y = meanp3_y/length(TestData);
    
    SampleTimestamp = 0:0.001:60;
    
    for i = 1:length(SampleTimestamp)
    FrictionData.Torque_x(i) = (meanp2_x + 2*meanp1_x*SampleTimestamp(i))*1.12e-5;
    FrictionData.RWS_x(i) = meanp3_x + meanp2_x*SampleTimestamp(i)+ meanp1_x*SampleTimestamp(i)^2;
    
    FrictionData.Torque_y(i) = (meanp2_y + 2*meanp1_y*SampleTimestamp(i))*1.12e-5;
    FrictionData.RWS_y(i) = meanp3_y + meanp2_y*SampleTimestamp(i) + meanp1_y*SampleTimestamp(i)^2;
    end
    

FrictionData.Fit_Torque_x = fit(FrictionData.RWS_x', FrictionData.Torque_x','poly3');
FrictionData.Fit_Torque_y = fit(FrictionData.RWS_y', FrictionData.Torque_y','poly3');

figure(1)
subplot(2,1,1)
plot(FrictionData.Fit_Torque_x, FrictionData.RWS_x, FrictionData.Torque_x)
subplot(2,1,2)
plot(FrictionData.Fit_Torque_y, FrictionData.RWS_y, FrictionData.Torque_y)
figure(2)
subplot(2,1,1)
plot(FitData(1).RWS_x, TestData(1).Timestamp, TestData(1).RWS_x)
subplot(2,1,2)
plot(FitData(1).RWS_y, TestData(1).Timestamp, TestData(1).RWS_y)
