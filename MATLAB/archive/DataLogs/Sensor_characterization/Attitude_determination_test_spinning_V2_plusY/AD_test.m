load 'AD_data.mat'

AngleStep = 20/180*3.1415;
TrueHeading = zeros(1,length(TestData));
HeadingError = zeros(1,length(TestData));
MeasuredHeading = zeros(1,length(TestData));
MeasuredHeading(1) = mean(TestData(1).Heading);
TrueHeading(1) = MeasuredHeading(1);
for i = 2:length(TestData)
    TrueHeading(i) = TrueHeading(i-1)+AngleStep;

    if TrueHeading(i) > 3.141592
        TrueHeading(i) = TrueHeading(i)-6.2832;
    end
    MeasuredHeading(i) = mean(TestData(i).Heading);
    HeadingError(i) = MeasuredHeading(i) - TrueHeading(i);
    if abs(HeadingError(i)) > 3.141592 
    if sign(HeadingError(i)) > 0
        HeadingError(i) = HeadingError(i)-6.2832;
    else
        HeadingError(i) = HeadingError(i)+6.2832;
    end
    end
end

figure(1)
plot(TrueHeading,HeadingError,'.')
figure(2)
plot(TrueHeading)
hold on
plot(MeasuredHeading,'g')
hold off