clear all

files = dir('Raw_data_files');
IdentificationColumns  = [37 38];
TestNumber = 0;

TestData.Description = [];
for k = 1:length(IdentificationColumns)
for i = 3:length(files)
    data = load(strcat('.\Raw_data_files\',  files(i).name));
    j = 2;
    while j < length(data(:,1))
        if data(j,IdentificationColumns(k)) == 0
            j = j+1;
        else
            if data(j-1,IdentificationColumns(k)) == 0 || j == 2
                TestNumber = TestNumber + 1;
                TestIndex = 1;
            end
    
    TestData(TestNumber).Mag_x(TestIndex) = data(j,1);
    TestData(TestNumber).Mag_y(TestIndex) = data(j,2);
    TestData(TestNumber).Mag_z(TestIndex) = data(j,3);
    TestData(TestNumber).Acc_x(TestIndex) = data(j,4);
    TestData(TestNumber).Acc_y(TestIndex) = data(j,5);
    TestData(TestNumber).Acc_z(TestIndex) = data(j,6);
    TestData(TestNumber).gyro1_x(TestIndex) = data(j,7);
    TestData(TestNumber).gyro1_y(TestIndex) = data(j,8);
    TestData(TestNumber).gyro1_z(TestIndex) = data(j,9);
    TestData(TestNumber).gyro2_x(TestIndex) = data(j,10);
    TestData(TestNumber).gyro2_y(TestIndex) = data(j,11);
    TestData(TestNumber).gyro2_z(TestIndex) = data(j,12);
    TestData(TestNumber).RWS_x(TestIndex) = data(j,13);
    TestData(TestNumber).RWS_y(TestIndex) = data(j,14);
    TestData(TestNumber).RWS_z(TestIndex) = data(j,15);
    TestData(TestNumber).K(TestIndex) = data(j,16);
    TestData(TestNumber).Ki_x(TestIndex) = data(j,17);
    TestData(TestNumber).Ki_y(TestIndex) = data(j,18);
    TestData(TestNumber).Ki_z(TestIndex) = data(j,19);
    TestData(TestNumber).P_x(TestIndex) = data(j,20);
    TestData(TestNumber).P_y(TestIndex) = data(j,21);
    TestData(TestNumber).P_z(TestIndex) = data(j,22);
    TestData(TestNumber).Target_Rate(TestIndex) = data(j,23);
    TestData(TestNumber).Target_Heading(TestIndex) = data(j,24);
    TestData(TestNumber).Target_RWS_x(TestIndex) = data(j,25);
    TestData(TestNumber).Target_RWS_y(TestIndex) = data(j,26);
    TestData(TestNumber).Target_RWS_z(TestIndex) = data(j,27);
    TestData(TestNumber).Mag_bias_x(TestIndex) = data(j,28);
    TestData(TestNumber).Mag_bias_y(TestIndex) = data(j,29);
    TestData(TestNumber).Mag_bias_z(TestIndex) = data(j,30);
    TestData(TestNumber).gyro1_bias_x(TestIndex) = data(j,31);
    TestData(TestNumber).gyro1_bias_y(TestIndex) = data(j,32);
    TestData(TestNumber).gyro1_bias_z(TestIndex) = data(j,33);
    TestData(TestNumber).gyro2_bias_x(TestIndex) = data(j,34);
    TestData(TestNumber).gyro2_bias_y(TestIndex) = data(j,35);
    TestData(TestNumber).gyro2_bias_z(TestIndex) = data(j,36);
    TestData(TestNumber).Control_mode_x(TestIndex) = data(j,37);
    TestData(TestNumber).Control_mode_y(TestIndex) = data(j,38);
    TestData(TestNumber).Control_mode_z(TestIndex) = data(j,39);
    TestData(TestNumber).Heading(TestIndex) = data(j,40);
    TestData(TestNumber).Pitch(TestIndex) = data(j,41);
    TestData(TestNumber).Roll(TestIndex) = data(j,39);
           
    TestIndex = TestIndex+1;
    j = j + 1;
        end
                
    end
end
end

TestsToDelete = [1, 3];

for i = 1:length(TestsToDelete)
    TestData(TestsToDelete(i)-(i-1))= [];
end

TruncationMatrix = [0, 0;
                    0, 0;
                    0, 0;
                    0, 0;
                    0, 0;
                    0, 0;
                    0, 0;
                    0, 0;
                    0, 0;
                    0, 0];

run('TruncateTests')
run('NameTheTests')

clear i j k IdentificationColumns TruncationMatrix TestsToDelete TestIndex TestNumber files data TestDescriptions
save 'Parsed_single_wheel_spinnng.mat'

Amplitude_x_on_x = range(TestData(1).Mag_x)/2;
Amplitude_x_on_y = range(TestData(1).Mag_y)/2;
Amplitude_x_on_z = range(TestData(1).Mag_z)/2;

Amplitude_y_on_x = range(TestData(2).Mag_x)/2;
Amplitude_y_on_y = range(TestData(2).Mag_y)/2;
Amplitude_y_on_z = range(TestData(2).Mag_z)/2;