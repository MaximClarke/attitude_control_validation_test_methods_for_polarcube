% FindSphere.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% FindSphere.m calculates the center and radius of a sphere that is
% represented by four points (3D coordinates) on its surface.

% For explanation consult: http://www.abecedarical.com/zenosamples/zs_sphere4pts.html
function [ r,center ] = FindSphere( SphereSample )


    SquareVector = SphereSample(:,1).^2+SphereSample(:,3).^2+SphereSample(:,2).^2;

    a = [SphereSample ones(4,1)];
    M11=det(a);
    
    a = [SquareVector SphereSample(:,2) SphereSample(:,3) ones(4,1)];
    M12=det(a);
    
    a = [SquareVector SphereSample(:,1) SphereSample(:,3) ones(4,1)];
    M13=det(a);
    
    a = [SquareVector SphereSample(:,1) SphereSample(:,2) ones(4,1)];
    M14=det(a);
    
    a = [SquareVector SphereSample(:,1) SphereSample(:,2) SphereSample(:,3)];
    M15=det(a);
    
    xo = 0.5*M12/M11;
    yo = -0.5*M13/M11;
    zo = 0.5*M14/M11;
    r = (xo^2+yo^2+zo^2-M15/M11)^0.5;
    center = [xo yo zo];
end

