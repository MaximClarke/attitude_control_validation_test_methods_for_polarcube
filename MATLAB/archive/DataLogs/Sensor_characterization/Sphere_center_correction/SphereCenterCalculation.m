% SphereCenterCalculation.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% SphereCenterCalculation.m takes a data set of magnetometer readings and
% attempts to calcualte the bias produced by local electronic components by
% finding the center of the sphere described by the data points. This is
% performed by calculating the sphere described by hundreds of sets of
% four data points and finding the one that has the lowest overall standard
% deviation in vector magnitude after subtracting the center from all data
% points.

%% Setup

% collect data set
load 'Parsed_sphere_center_correction.mat'

% isolate magnetometer data
OriginalSet = [TestData(1).Mag_x', TestData(1).Mag_y', TestData(1).Mag_z'];

% Butterworth Filter

SampleFrequency = 160;
CutoffSpeed = 300; % Nyquist speed at sample frequency of 160 is 500 rad/s
CutoffFrequency = CutoffSpeed/2/3.242592;
NormalizedCutoffFrequency = CutoffFrequency*2/SampleFrequency;

[BFa,BFb] = butter(2,NormalizedCutoffFrequency);

ButterFiltered = [filter(BFa,BFb,OriginalSet(:,1)) filter(BFa,BFb,OriginalSet(:,2)) filter(BFa,BFb,OriginalSet(:,3))];

ButterFiltered = ButterFiltered((10:length(OriginalSet(:,1))),:);
% Moving average filter

BinWidth = 2;
MADataPoints = (BinWidth+1):(length(OriginalSet(:,1))-BinWidth);
MovingAveraged = zeros(length(MADataPoints),3);

for i = MADataPoints
    MovingAveraged(i-BinWidth,:) = [mean(OriginalSet((i-BinWidth):(i+BinWidth),1)) mean(OriginalSet((i-BinWidth):(i+BinWidth),2)) mean(OriginalSet((i-BinWidth):(i+BinWidth),3))];
end

%chose which dataset will be corrected,
UncorrectedSet = OriginalSet;
% Empty vectors

BestCenter = [0 0 0];

DataDivision = 9; % Minimum value
SampleSetNumber = (DataDivision-1)/4;
BestStd = 1000; %intentionally large

%% Calculate
iterations = length(TestData(1).Mag_x)/16;
for j = 1:iterations
    % Define the selection of data
    DataDivision = DataDivision+4;
    SampleDistance = round(length(UncorrectedSet(:,1))/DataDivision)-1;
    SampleSetNumber = (DataDivision-1)/4;
    
    radii = zeros(SampleSetNumber,1);
centers = zeros(SampleSetNumber,3);

for i = 1:SampleSetNumber
    % Construct a matrix for input to the FindSphere function 
SphereSample= [UncorrectedSet(i*SampleDistance,1) UncorrectedSet(i*SampleDistance,2) UncorrectedSet(i*SampleDistance,3);...
                UncorrectedSet((i+SampleSetNumber)*SampleDistance,1) UncorrectedSet((i+SampleSetNumber)*SampleDistance,2) UncorrectedSet((i+SampleSetNumber)*SampleDistance,3);...
                UncorrectedSet((i+2*SampleSetNumber)*SampleDistance,1) UncorrectedSet((i+2*SampleSetNumber)*SampleDistance,2) UncorrectedSet((i+2*SampleSetNumber)*SampleDistance,3);...
                UncorrectedSet((i+3*SampleSetNumber)*SampleDistance,1) UncorrectedSet((i+3*SampleSetNumber)*SampleDistance,2) UncorrectedSet((i+3*SampleSetNumber)*SampleDistance,3)];


% Collect sphere calculation data
[radii(i),centers(i,:)]= FindSphere(SphereSample);
end

meanradii=mean(radii);
stdradii=std(radii);

% Remove spheres with abnormal radii
i=1;
while i <= length(radii)
    if abs(radii(i)-meanradii) >= stdradii
        radii(i) = [];
        centers(i,:) = [];
    else
    i=i+1;
    end
end


% Use the mean center value to move the dataset about the origin 
AverageCenter = [mean(centers(:,1)) mean(centers(:,2)) mean(centers(:,3))];
CorrectedSet = [UncorrectedSet(:,1)-AverageCenter(1) UncorrectedSet(:,2)-AverageCenter(2) UncorrectedSet(:,3)-AverageCenter(3)];

% If the new center produces the lowest standard deviation in vector
% magnitude, then keep it.

magnitude = (CorrectedSet(:,1).^2+CorrectedSet(:,2).^2+CorrectedSet(:,3).^2).^0.5;
if std(magnitude) < BestStd
    BestCenter = AverageCenter;
    BestStd = std(magnitude);
end
end

% Displace the dataset by the best center value
CorrectedSet = [(UncorrectedSet(:,1)-BestCenter(1)) (UncorrectedSet(:,2)-BestCenter(2)) (UncorrectedSet(:,3)-BestCenter(3))];

    


magnitude = (CorrectedSet(:,1).^2+CorrectedSet(:,2).^2+CorrectedSet(:,3).^2).^0.5;
std(magnitude)

%% Plot

figure(1)
plot3(CorrectedSet(:,1),CorrectedSet(:,2),CorrectedSet(:,3),'.')
axis equal
axis vis3d
title('Corrected Magnetometer readings')
zlabel('Z')
ylabel('y')
xlabel('x')

figure(3)
plot(magnitude)
title('Magnitude of corrected magnetometer readings')
xlabel('Data point #')
ylabel('Vector magnitude')