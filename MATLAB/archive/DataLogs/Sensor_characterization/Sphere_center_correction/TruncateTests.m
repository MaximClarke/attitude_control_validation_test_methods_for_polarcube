

for i = 1:length(TruncationMatrix(:,1))
    if TruncationMatrix(i,1) == 0 && TruncationMatrix(i,2) == 0
    else
    if TruncationMatrix(i,1) == 0
        LowerBound = 1;
    else 
        LowerBound = TruncationMatrix(i,1);
    end
    
    if TruncationMatrix(i,2) == 0
        UpperBound = length(TestData(i).gyro1_x);
    else
        UpperBound = TruncationMatrix(i,2);
    end
        
    
    TestData(i).Mag_x = TestData(i).Mag_x(LowerBound:UpperBound);
    TestData(i).Mag_y = TestData(i).Mag_y(LowerBound:UpperBound);
    TestData(i).Mag_z = TestData(i).Mag_z(LowerBound:UpperBound);
    TestData(i).Acc_x = TestData(i).Acc_x(LowerBound:UpperBound);
    TestData(i).Acc_y = TestData(i).Acc_y(LowerBound:UpperBound);
    TestData(i).Acc_z = TestData(i).Acc_z(LowerBound:UpperBound);
    TestData(i).gyro1_x = TestData(i).gyro1_x(LowerBound:UpperBound);
    TestData(i).gyro1_y = TestData(i).gyro1_y(LowerBound:UpperBound);
    TestData(i).gyro1_z = TestData(i).gyro1_z(LowerBound:UpperBound);
    TestData(i).gyro2_x = TestData(i).gyro2_x(LowerBound:UpperBound);
    TestData(i).gyro2_y = TestData(i).gyro2_y(LowerBound:UpperBound);
    TestData(i).gyro2_z = TestData(i).gyro2_z(LowerBound:UpperBound);
    TestData(i).RWS_x = TestData(i).RWS_x(LowerBound:UpperBound);
    TestData(i).RWS_y = TestData(i).RWS_y(LowerBound:UpperBound);
    TestData(i).RWS_z = TestData(i).RWS_z(LowerBound:UpperBound);
    TestData(i).K = TestData(i).K(LowerBound:UpperBound);
    TestData(i).Ki_x = TestData(i).Ki_x(LowerBound:UpperBound);
    TestData(i).Ki_y = TestData(i).Ki_y(LowerBound:UpperBound);
    TestData(i).Ki_z = TestData(i).Ki_z(LowerBound:UpperBound);
    TestData(i).P_x = TestData(i).P_x(LowerBound:UpperBound);
    TestData(i).P_y = TestData(i).P_y(LowerBound:UpperBound);
    TestData(i).P_z = TestData(i).P_z(LowerBound:UpperBound);
    TestData(i).Target_Rate = TestData(i).Target_Rate(LowerBound:UpperBound);
    TestData(i).Target_Heading = TestData(i).Target_Heading(LowerBound:UpperBound);
    TestData(i).Target_RWS_x = TestData(i).Target_RWS_x(LowerBound:UpperBound);
    TestData(i).Target_RWS_y = TestData(i).Target_RWS_y(LowerBound:UpperBound);
    TestData(i).Target_RWS_z = TestData(i).Target_RWS_z(LowerBound:UpperBound);
    TestData(i).Mag_bias_x = TestData(i).Mag_bias_x(LowerBound:UpperBound);
    TestData(i).Mag_bias_y = TestData(i).Mag_bias_y(LowerBound:UpperBound);
    TestData(i).Mag_bias_z = TestData(i).Mag_bias_z(LowerBound:UpperBound);
    TestData(i).gyro1_bias_x = TestData(i).gyro1_bias_x(LowerBound:UpperBound);
    TestData(i).gyro1_bias_y = TestData(i).gyro1_bias_y(LowerBound:UpperBound);
    TestData(i).gyro1_bias_z = TestData(i).gyro1_bias_z(LowerBound:UpperBound);
    TestData(i).gyro2_bias_x = TestData(i).gyro2_bias_x(LowerBound:UpperBound);
    TestData(i).gyro2_bias_y = TestData(i).gyro2_bias_y(LowerBound:UpperBound);
    TestData(i).gyro2_bias_z = TestData(i).gyro2_bias_z(LowerBound:UpperBound);
    TestData(i).Control_mode_x = TestData(i).Control_mode_x(LowerBound:UpperBound);
    TestData(i).Control_mode_y = TestData(i).Control_mode_y(LowerBound:UpperBound);
    TestData(i).Control_mode_z = TestData(i).Control_mode_z(LowerBound:UpperBound);
    TestData(i).MRP_x = TestData(i).MRP_x(LowerBound:UpperBound);
    TestData(i).MRP_y = TestData(i).MRP_y(LowerBound:UpperBound);
    TestData(i).MRP_z = TestData(i).MRP_z(LowerBound:UpperBound);
    TestData(i).Error_MRP_x = TestData(i).Error_MRP_x(LowerBound:UpperBound);
    TestData(i).Error_MRP_y = TestData(i).Error_MRP_y(LowerBound:UpperBound);
    TestData(i).Error_MRP_z = TestData(i).Error_MRP_z(LowerBound:UpperBound);
    TestData(i).Filter_mode = TestData(i).Filter_mode(LowerBound:UpperBound);
    TestData(i).Heading = TestData(i).Heading(LowerBound:UpperBound);
    TestData(i).Pitch = TestData(i).Pitch(LowerBound:UpperBound);
    TestData(i).Roll = TestData(i).Roll(LowerBound:UpperBound);
    TestData(i).Torque_command_x = TestData(i).Torque_command_x(LowerBound:UpperBound);
    TestData(i).Torque_command_y = TestData(i).Torque_command_x(LowerBound:UpperBound);
    TestData(i).Torque_command_z = TestData(i).Torque_command_x(LowerBound:UpperBound);
    TestData(i).Timestamp = TestData(i).Timestamp(LowerBound:UpperBound);
    end
end

clear LowerBound UpperBound