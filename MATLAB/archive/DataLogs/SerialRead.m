% SerialRead.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% SerialRead.m reads attitude determination data sent from the arduino test
% model, dispalys them on the MATLAB command line live, then saves the data
% log to a file.

%% Setup

% Open serial port
s = serial('COM5');
fopen(s);

% Empty data matrix will be filled
data = [];


SamplePeriod = 0.01;
OffWait = 10;
CountLimit = OffWait / SamplePeriod;
OffCount = 0;
FileReady = false;
AnyData = false;
i = 1;
DataLabels = {'timestamp'; 'magnetometer x'; 'magnetometer y'; 'magnetometer z'; 'accelerometer x'; 'accelerometer y'; 'accelerometer z'; 'gyro x'; 'gyro y'; 'gyro z'; 'Heading from Fio'; 'Heading from MATLAB'; 'Heading from DCM';'Pitch from Fio';'Pitch from MATLAB'; 'Pitch from DCM'; 'Roll from Fio'; 'Roll from MATLAB'; 'Roll from DCM'};

%% Read

% clear serial port
read = fscanf(s,'%f');
while 1 == 1

    BytesAvailable = s.BytesAvailable;
    % Check if data has been recieved and Arduino has been turned off
    if OffCount >= CountLimit && AnyData == true
        % If so, the exit
        break
    % Check if data is available to read
    elseif BytesAvailable == 0
    % if not then don't read
           OffCount = OffCount+1;
    else
    
    % Check if a command file has been uploaded (for op-code purposes in
    % the future)
    if exist('CommandFile.txt','file') == 2
        % If so, the read the command, execute it and delete the command file
       CommandFile =  fopen('CommandFile.txt');
       Command = fscanf(CommandFile,'%s');
       fclose(CommandFile);
       eval(Command);
       delete('CommandFile.txt')
    end
    
    % Read the serial port
    read = fscanf(s,'%f');
    
    %The following section re-calculates attitude determination to
    %verify the code on the arduino.
    
    
    MagVector = [read(2); read(3); read(4)];
    AccVector = [read(5); read(6); read(7)];
    

    GRAVvec = -AccVector;
    dueWEST = cross(MagVector,GRAVvec);
    
    % Normalize
    dueWEST = dueWEST/norm(dueWEST);
    GRAVvec = GRAVvec/norm(GRAVvec);
    
    XcrossGRAV = cross([1; 0; 0],GRAVvec);
    
    % Calculate pitch
    Pitch = acos(norm(XcrossGRAV));
    if GRAVvec(1) >0
        Pitch = -Pitch;
    end
    
    % Calculate Heading
    XcrossGRAV = XcrossGRAV/norm(XcrossGRAV);
    Heading = acos(dot(dueWEST,XcrossGRAV));
    if dueWEST(1)*XcrossGRAV(2) < 0
        Heading = -Heading;
    end
    
    %Calculate Roll
    Roll = acos(XcrossGRAV(2));
    if XcrossGRAV(3) < 0
        Roll = -Roll;
    end
    
    % Re-calculate attitude with DCM for further verification
    dueNORTH = cross(GRAVvec,dueWEST);
    DCM = [dueNORTH dueWEST -GRAVvec];
    DCMPitch = asin(DCM(1,3));
    DCMHeading = asin(DCM(1,2)/cos(DCMPitch));
    DCMRoll = -asin(DCM(2,3)/cos(DCMPitch));
     
    % Add MATLAB derived attitude to output vector
    read = [read(1:11); Heading; DCMHeading; read(12); Pitch; DCMPitch; read(13); Roll; DCMRoll ];
    
    % Display output vector with labels
    DataDisplay = [DataLabels cellstr(num2str(read))];
    disp(DataDisplay)
    
    % Add output to data matrix
    data = [data; read'];
    
    AnyData = true;

    OffCount = 0;
    end
    pause(SamplePeriod);

end

fclose(s);


%% Save to file (typically happens when the XBee is turned off)
while FileReady == false
    % create file name
    FileName = ['SerialLogs\SerialLog' num2str(i) '.txt'];
    % check for availability
    if exist(FileName,'file') == 2
        i = i+1;
    else
        FileReady = true;
        % Write to file
        dlmwrite(FileName,data);
    end
end
EndMessage = ['data written to ' FileName];
disp(EndMessage)