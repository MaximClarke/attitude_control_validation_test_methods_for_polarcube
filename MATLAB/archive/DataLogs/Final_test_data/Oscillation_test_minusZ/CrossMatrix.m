% CrossMatrix.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% CrossMatrix.m converts a vector to a cross-product matrix, allowing cross
% product operations to be conducted on matrices. the finction is analogous
% to tilde.m and skew.m in other Space Grant matlab projects.

function [ Asquiggle ] = CrossMatrix( A )

Asquiggle = [0 -A(3) A(2);
            A(3) 0 -A(1);
            -A(2) A(1) 0];

end

