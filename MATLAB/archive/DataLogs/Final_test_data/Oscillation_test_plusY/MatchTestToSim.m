% MatchTestToSim.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% MatchTestToSim.m plots four sets of test data to output of the
% StringTestbedModelFN function. This is used to find the parameters that
% provide a best fit of the string testbed model to the measured test data.
load 'AD_Results.mat'
load 'Oscillation_data.mat'

Prediction = false;

% Set equilibrium angle spring constant and damping constant
SpringConstant = 0.0000192;
DampingConstant = 0.000026;
EquilibriumAngle = -2.31;

% Moment of inertia of the Test model body
%IB=[0.002106 -1.1e-5 -0.00012;
 %   -1.1e-5 0.00214 1.58e-5;
  %  -0.00012 1.58e-5 0.000975];

IB=[0.002106 0 0;
    0 0.00214 0;
    0 0 0.000975];

K_pred = diag([0; 0; 0]);
P_pred = diag([0; 0; 0]);
Ki_pred = diag([0; 0; 0]);

K_fit = diag([0; 0; 0]);
P_fit = diag([0; 0; 0]);
Ki_fit = diag([0; 0; 0]);

TargetTheta = 0;

% For the set of test data files
for i = 1:length(TestData)
    % Apply equilibrium angle
    
    HeadingCorrections = interp1(ADtestResults(1).MeasuredHeading, ADtestResults(1).HeadingError, TestData(i).Heading);
    
    DeflectionAngles = TestData(i).Heading-EquilibriumAngle;
    CorrectedDeflectionAngles  = DeflectionAngles - HeadingCorrections;

    for j = 1:length(TestData(i).Heading)
        if DeflectionAngles(j) < (-3.141592)
       DeflectionAngles(j) = DeflectionAngles(j)+2*3.141592;
        elseif DeflectionAngles(j) > (3.141592)
       DeflectionAngles(j) = DeflectionAngles(j)-2*3.141592;
        end
        if CorrectedDeflectionAngles(j) < (-3.141592)
       CorrectedDeflectionAngles(j) = CorrectedDeflectionAngles(j)+2*3.141592;
        elseif CorrectedDeflectionAngles(j) > (3.141592)
       CorrectedDeflectionAngles(j) = CorrectedDeflectionAngles(j)-2*3.141592;
        end
    end
    
    
    
    % Collect string testbed model output 
    [simtime_fit,theta_fit] = StringTestbedModelFN(CorrectedDeflectionAngles(1),TargetTheta,K_fit,P_fit,Ki_fit,IB,SpringConstant, DampingConstant , round(TestData(i).Timestamp(length(TestData(i).Timestamp))));
    
    if Prediction == true
        [simtime_pred,theta_pred] = StringTestbedModelFN(CorrectedDeflectionAngles(1),TargetTheta,K_pred,P_pred,Ki_pred,IB,SpringConstant, DampingConstant , round(TestData(i).Timestamp(length(TestData(i).Timestamp))));
    else
        simtime_pred = NaN;
        theta_pred = NaN;
    end
    % Keep test data within +-pi

    % Plot test data and model output
    figure(4)
    subplot(length(TestData),1,i)
    plot(TestData(i).Timestamp,DeflectionAngles,':', TestData(i).Timestamp,CorrectedDeflectionAngles,simtime_fit,theta_fit,simtime_pred,theta_pred)
    TitleString = 'Heading measured from tests (blue and green) plotted against best fitting model of string dynaics (red)';
    if Prediction == true
        PredictionString = 'and a prediction based on control law configuration (Red)';
    else
        PredictionString = '';
    end
    if i == 1
        title({TitleString;PredictionString})
    end
    if i == length(TestData)
        xlabel('Time (s)')
    end
    ylabel('Heading (rad)')
end