data = dlmread('LOG6.txt');
MeasuredHeadingColumn = 3;
TargetHeadingColumn = 4;
TimestampColumn = 1;


MeasuredHeadings = data(:,MeasuredHeadingColumn);
TargetHeadings = data(:,TargetHeadingColumn);
Timestamps = data(:,TimeStampColumn);

DataLength = length(Timestamps);

HeadingErrors = TargetHeadings - MeasuredHeadings;

OnTarget = 0;

for i = DataLength
    if HeadingErrors < TargetThreshold
        OnTarget = OnTarget+1;
    else
        OnTarget = 0;
    end
    
    if OnTarget >= TargetAcquisition
        TargetAcquired = i-TargetAcquisition;
        break
    end
end

TimeToAcquisition = Timestamps(TargetAcquired);

PointingErrors = HeadingErrors(TargetAcquired:DataLength); 

PointingTimestamps = TimeStamps(TargetAcquired:DataLength);

PointingTimestampsNMinusOne = [NaN PointingTimeStamps(1:length(PointingTimeStamps)-1)];
PointingTimestampDifferentials = PointingTimestamps-PointingTimestampsNMinusOne;

PointingRates = PointingErrors ./ PointingTimestampDifferentials;

PointingAccuracy = range(PointingErrors);
PointingStability = max(abs(PointingRates));

PlotTargetAcquisition = [TimeToAcquisition TimeToAcquisition; min(MeasuredHeadings) max(MeasuredHeadings)];

figure(1)
plot(Timestamps, MeasuredHeadings, Timestamps, TargetHeadings, PlotTargetAcquisition(1,:), PlotTargetAcquisition(2,:))
title('Measured heading response plotted against target heading ')
xlabel('Time (s)')
ylabel ('Heading (rad)')
legend('Measured heading response','Target heading','Target acquisition point')
figure(2)
plot(PointingTimestamps, PointingErrors)
title('Heading error at pointing target ')
xlabel('Time (s)')
ylabel ('Heading error (rad)')
