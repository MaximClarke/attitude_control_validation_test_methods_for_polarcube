% ADtest.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% ADtest is designed to produce a set of data that will indicate the
% performance of the attitude determination method implemented on the
% test model. It is intended to be used in conjunction with the attitude
% determination test rig. 

% The script will tell the user to set a fixed
% known attitude angle, then input from the user will tell the script to
% collect a set of attitude determination data points. This repeats for a
% full rotation. The mean value, standard deviation of the determined Euler
% angles are logged alongside the "true heading" and attitude error and saved to a file. 

% The script assumes that the first data point is accurate and calculates the
% true heading values incrementally from that point.


% Define the serial port
s = serial('COM5');

%% Define the data set that will be collected

%Angular resolution of measurements
AngleStep = 20;
NumberOfMeasurements = 360/AngleStep;
Data = zeros(NumberOfMeasurements,8);

%Sample set for each easurement
SamplesPerMeasurement = 10;
SampleSet = zeros(SamplesPerMeasurement,3);


%% Initial measurement

% true attitude command
input('Set heading to 0 deg.')

% Open and clear serial port
    fopen(s);
    read = fscanf(s,'%f');
    
% Take measurement
for k = 1:SamplesPerMeasurement
    read = fscanf(s,'%f');
    SampleSet(k,:) = read(11:13)';
    
end

% Close serial port 
fclose(s);

% Write first data point (the first measurement is assumed to be accurate >> error of first measurement is 0)
ReferenceAngle = mean(SampleSet(:,1));
% Data = ['True attitude' 'Heading error' 'Measured heading' 'Heading uncertainty' 'Measured Pitch' 'Pitch uncertainty' 'Measured roll' 'Roll uncertainty']
Data(1,:) = [ReferenceAngle 0 ReferenceAngle std(SampleSet(:,1)) mean(SampleSet(:,2)) std(SampleSet(:,2)) mean(SampleSet(:,3)) std(SampleSet(:,3))];

%% Take subsequent measurements
for i = 2:(NumberOfMeasurements)
    
    % True heading with respect to the first measurement
    TrueHeading = mod(ReferenceAngle*180/3.141592+AngleStep*(i-1),360)/180*3.141592;
    if TrueHeading > 3.141592
        TrueHeading = TrueHeading-6.2832;
    end
    % True attitude 
    input(['Set heading to ' num2str(AngleStep*(i-1)) ' deg.'])
    
    % Open and clear serial port
    fopen(s);
    read = fscanf(s,'%f');
    
    % Take measurement
    for j = 1:SamplesPerMeasurement
    read = fscanf(s,'%f');
    SampleSet(j,:) = read(11:13)';
    end
    
    % Close serial port 
    fclose(s);
    
    % Write data point
    Heading = mean(SampleSet(:,1));
    HeadingError = Heading-TrueHeading;
    
    if abs(HeadingError) > 3.141592 
    if sign(HeadingError) > 0
        HeadingError = HeadingError-6.2832;
    else
        HeadingError = HeadingError+6.2832;
    end
    end
    
    % Data = ['True attitude' 'Heading error' 'Measured heading' 'Heading uncertainty' 'Measured Pitch' 'Pitch uncertainty' 'Measured roll' 'Roll uncertainty']
    Data(i,:) = [TrueHeading HeadingError Heading  std(SampleSet(:,1)) mean(SampleSet(:,2)) std(SampleSet(:,2)) mean(SampleSet(:,3)) std(SampleSet(:,3))];
    
end

%% Plot Heading error
figure(1)
plot(Data(:,1),Data(:,2),'.')
title('Heading error as a function of true heading')
xlabel('True heading (deg)')
ylabel('Heading error (deg)')


%% Write to file
FileReady = false;
i=0;
while FileReady == false
    % Construct file name
    FileName = ['C:\Users\Maxim\Documents\Thesis\Git\MATLAB\DataLogs\ADTestLogs\ADTestLog' num2str(i) '.txt'];
    
    % Check if that name is available
    if exist(FileName,'file') == 2
        i = i+1;
    else
        FileReady = true;
    % Write
        dlmwrite(FileName,Data);
    end
end

EndMessage = ['data written to ' FileName];
disp(EndMessage)