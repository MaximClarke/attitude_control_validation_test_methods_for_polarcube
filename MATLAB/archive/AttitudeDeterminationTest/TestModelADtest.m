% ADtest.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% ADtest is designed to produce a set of data that will indicate the
% performance of the attitude determination method implemented on the
% test model. It is intended to be used in conjunction with the attitude
% determination test rig. 

% The script will tell the user to set a fixed
% known attitude angle, then input from the user will tell the script to
% collect a set of attitude determination data points. This repeats for a
% full rotation. The mean value, standard deviation of the determined Euler
% angles are logged alongside the "true heading" and attitude error and saved to a file. 

% The script assumes that the first data point is accurate and calculates the
% true heading values incrementally from that point.



DirectoryPath = 'C:\Users\Maxim\Documents\Thesis\Git\MATLAB\DataLogs\ADTestLogs\TestFilesFromGui';
Directory = dir(DirectoryPath);
NuberOfFiles = length(Directory);

HeadingColumn = 2;
PitchColumn = 3;
Rollcolumn = 4;

AngleStep = 20;
%% Initial measurement
data
for i = 0:1:(NumberOfFiles-1)
    OpenFile = dlmread([DirectoryPath num2string(i*AngleStep) '.txt']);
    Headings = OpenFile(:,HeadingColumn);
    Pitches = OpenFile(:,PitchColumn);
    Rolls = OpenFile(:,RollColumn);
    if i == 0
        ReferenceAngle = mean(Headings);
    end
    TrueHeading = ReferenceAngle+i*Anglestep/180*3.141592;
    HeadingError = Mean(Headings) - TrueHeading;
    
    
% true attitude command

    % Data = ['True attitude' 'Heading error' 'Measured heading' 'Heading uncertainty' 'Measured Pitch' 'Pitch uncertainty' 'Measured roll' 'Roll uncertainty']
    Data(i+1,:) = [TrueHeading HeadingError mean(Headings)  std(Headings) mean(Pitches)) std(Pitches) mean(Rolls) std(Rolls)];
    
end

%% Plot Heading error
figure(1)
plot(Data(:,1),Data(:,2),'.')
title('Heading error as a function of true heading')
xlabel('True heading (deg)')
ylabel('Heading error (deg)')


%% Write to file
FileReady = false;
i=0;
while FileReady == false
    % Construct file name
    FileName = ['C:\Users\Maxim\Documents\Thesis\Git\MATLAB\DataLogs\ADTestLogs\GuiADTestLog' num2str(i) '.txt'];
    
    % Check if that name is available
    if exist(FileName,'file') == 2
        i = i+1;
    else
        FileReady = true;
    % Write
        dlmwrite(FileName,Data);
    end
end

EndMessage = ['data written to ' FileName];
disp(EndMessage)