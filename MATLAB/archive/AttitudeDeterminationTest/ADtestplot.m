% ADtestplot.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% ADtestplot serves to do data analysis on a saved dataset from performing
% an attitude determination test with ADtest.m. For now, all that it does
% is fit a sinusoid to the heading error data, this could eventually be used for
% feed-forward heading error compensation.

% Collect dataset
data = dlmread('C:\Users\Maxim\Documents\Thesis\Git\MATLAB\DataLogs\ADTestLogs\ADTestLog8.txt');
data2 = dlmread('C:\Users\Maxim\Documents\Thesis\Git\MATLAB\DataLogs\ADTestLogs\ADTestLog9.txt');
data3 = dlmread('C:\Users\Maxim\Documents\Thesis\Git\MATLAB\DataLogs\ADTestLogs\ADTestLog10.txt');
data4 = dlmread('C:\Users\Maxim\Documents\Thesis\Git\MATLAB\DataLogs\ADTestLogs\ADTestLog11.txt');
data5 = dlmread('C:\Users\Maxim\Documents\Thesis\Git\MATLAB\DataLogs\ADTestLogs\ADTestLog12.txt');
data6 = dlmread('C:\Users\Maxim\Documents\Thesis\Git\MATLAB\DataLogs\ADTestLogs\ADTestLog13.txt');
% Fit sinusoid to the heading error signal with respect to the true
% heading
TrueHeading = data(:,1);
ADError = data(:,2);
ADErrorFitT = fit(TrueHeading,ADError,'poly4');

% Fit sinusoid to the heading error signal with respect to the measured
% heading
MeasuredHeading = data(:,3);
ADErrorFitM = fit(TrueHeading,ADError,'poly4');

% Plot data
figure(1)
subplot(2,1,1)
plot(ADErrorFitT,TrueHeading,ADError);
title('Heading error and best sinusoidal fit with respect to heading')
xlabel('True heading')
ylabel('Heading error')
subplot(2,1,2)
plot(ADErrorFitM,MeasuredHeading,ADError);
xlabel('Measured heading')
ylabel('Heading error')

figure(2)
subplot(2,3,1)
plot(TrueHeading,ADError,'.');
title('Heading error and best sinusoidal fit with respect to heading')
xlabel('True heading')
ylabel('Heading error')
subplot(2,3,4)
plot(data2(:,1),data2(:,2),'.');
xlabel('Measured heading')
ylabel('Heading error')
subplot(2,3,2)
plot(data3(:,1),data3(:,2),'.');
xlabel('Measured heading')
ylabel('Heading error')
subplot(2,3,5)
plot(data4(:,1),data4(:,2),'.');
xlabel('Measured heading')
ylabel('Heading error')
subplot(2,3,3)
plot(data5(:,1),data5(:,2),'.');
xlabel('Measured heading')
ylabel('Heading error')
subplot(2,3,6)
plot(data6(:,1),data6(:,2),'.');
xlabel('Measured heading')
ylabel('Heading error')

