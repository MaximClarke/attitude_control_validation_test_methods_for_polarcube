t = 100;
dt = 0.1;
time = dt:dt:t;
outputlength = t/dt;

mRW = 0.0265;
rRW = 0.025;
hRW = 0.005;
JRW=0.5*mRW*rRW^2;

k=1000;

mb = 1.4;
hb = 10;
wb = 10;
db = 33;
IB=[mb*(hb^2+db^2)/12 0 0;
    0 mb*(wb^2+db^2)/12 0;
    0 0 mb*(hb^2+wb^2)/12];

SpinUpTime = 10;
MirrorSpinRate = 2*pi;

mM = 0.0636;
rM = 0.05;
hM = 0.003;
JM = 0.5*mM*rM^2;

SpinUpTorque = MirrorSpinRate*JM/SpinUpTime;

MirrorSpinAxis = [1; 1; 1];
MirrorSpinAxis = MirrorSpinAxis/norm(MirrorSpinAxis);

omegaRW = zeros(3,outputlength);
omegaBB = zeros(3,outputlength);
omegaBBdot = zeros(3,outputlength);
omegaNB = zeros(3,outputlength);
RNB = zeros(3,3,outputlength);

Toutside = zeros(3,outputlength);
Tgyro = zeros(3,outputlength);
Tilt = zeros(2,outputlength);

omegaBB(:,1) = [0;0;0];
RNB(:,:,1) = [1 0 0; 0 1 0; 0 0 1];
omegaNB(:,1) = RNB(:,:,1)*omegaBB(:,1);

for i = 1:(SpinUpTime/dt)
Toutside(:,i) = MirrorSpinAxis * SpinUpTorque;
end



for i=2:length(time)
   
    RNB(:,:,i)= RNB(:,:,i-1)+ dt*(CrossMatrix(omegaNB(:,i-1))*RNB(:,:,(i-1)));
    RNB(:,:,i)= GSNormalize(RNB(:,:,i),1,2,3);
    omegaBB(:,i) = omegaBB(:,i-1)+dt*omegaBBdot(:,i-1);
    omegaNB(:,i) = RNB(:,:,i)*omegaBB(:,i);
     Tgyro(:,i)=-CrossMatrix(omegaBB(:,i))*(JRW*omegaRW(:,i));
    omegaBBdot(:,i) = IB\(Tgyro(:,i) + CrossMatrix(omegaBB(:,i))*IB*omegaBB(:,i) +Toutside(:,i));
    
end
xaxis  = reshape(RNB(:,3,:),3,length(time));
figure(1)
subplot(3,1,1)
plot(time, omegaNB(1,:))
subplot(3,1,2)
plot(time, omegaNB(2,:))
subplot(3,1,3)
plot(time, omegaNB(3,:))

figure(2)
subplot(3,1,1)
plot(time, xaxis(1,:))
subplot(3,1,2)
plot(time, xaxis(2,:))
subplot(3,1,3)
plot(time, xaxis(3,:))