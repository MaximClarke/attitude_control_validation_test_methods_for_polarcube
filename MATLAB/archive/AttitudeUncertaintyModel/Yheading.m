% Yheading.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% Yheading.m calculates heading of the body x axis with respect to magnetic 
% north provided readings from a magnetometer and
% accelerometer. Yheading only works if the y body axis is pointed above
% the horizontal

function [Heading] = Yheading(MagFieldVector, GravityVector)
% Calculate magnetic west
DueWest = cross(MagFieldVector, GravityVector);
DueWest = DueWest/norm(DueWest);

% Cross product of the x body axis and gravity
XcrossGrav = cross([1 0 0], GravityVector);
XcrossGrav = XcrossGrav/norm(XcrossGrav);

% Calculate heading
Heading = acos(dot(DueWest, XcrossGrav));
% Positive or neagitve angle (This is the reason you need 3 heading
% formulas)
if DueWest(1)*XcrossGrav(3) > 0
    Heading = -Heading;
end
% 0 to 2*pi
if Heading < 0
    Heading = 2*3.141592+Heading;
end
% convert to deg
Heading = Heading/3.141592*180;
end