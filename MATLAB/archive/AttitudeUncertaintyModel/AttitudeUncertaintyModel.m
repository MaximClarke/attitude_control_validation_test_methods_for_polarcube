% AttitudeUncertaintyModel.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% AttitudeUncertaintyModel uses a monte-carlo type method to calculate uncertainty 
% in heading measurement as a result of disturbance on the magnetometer measurements produced by the
% rotating permanent magnets in the reaction wheel motors. 

% For a set of headings and speeds of the parallel reaction wheel, the
% script calculates heading several times with noise on the magnetometer
% input based on the results calculated from ReactionWheelAliasingModelFN
% and determines the RMS of the resultant heading error.

%% Set parameters
RotationAxis = 'y'; % body axis about which rotation occurs ('x', 'y' or 'z')
AmbientMagnetic = [440 0 -630]';  % Ambient magnetic field vector in Earth-reference
InertialGravity = [0 0 -256]';    % Ambient gravity field vector in Earth-reference

SampleFrequency = 160; % Hz
MeasurementPeriod = 1; % s
MeasurementNumber = 100; 
MaxReactionWheelSpeed = 999; % rad/s
SecondaryRWSpeed = 150; %rad/s

% Measured influence of the reaction wheels on magnetoeter measurement
AmplitudeXonX = 200;
AmplitudeXonY = 100;
AmplitudeXonZ = 400;
AmplitudeYonX = 350;
AmplitudeYonY = 250;
AmplitudeYonZ = 50;
AmplitudeZonX = 500;
AmplitudeZonY = 200;
AmplitudeZonZ = 300;

% Collect magnetoeter uncertainty data
XMagErrorRMSdata = ReactionWheelAliasingModelFN(SampleFrequency, MeasurementPeriod, MeasurementNumber,MaxReactionWheelSpeed, RotationAxis, AmplitudeXonX, AmplitudeYonX, AmplitudeZonX, SecondaryRWSpeed, SecondaryRWSpeed, SecondaryRWSpeed);
YMagErrorRMSdata = ReactionWheelAliasingModelFN(SampleFrequency, MeasurementPeriod, MeasurementNumber,MaxReactionWheelSpeed, RotationAxis, AmplitudeXonY, AmplitudeYonY, AmplitudeZonY, SecondaryRWSpeed, SecondaryRWSpeed, SecondaryRWSpeed);
ZMagErrorRMSdata = ReactionWheelAliasingModelFN(SampleFrequency, MeasurementPeriod, MeasurementNumber,MaxReactionWheelSpeed, RotationAxis, AmplitudeXonZ, AmplitudeYonZ, AmplitudeZonZ, SecondaryRWSpeed, SecondaryRWSpeed, SecondaryRWSpeed);

% Empty vector
MeasurementError = zeros(1,MeasurementNumber);

% Define the orientation of the test model body with respect to
% Earth-reference and the rotation and attitude determination functions to
% use.
if RotationAxis == 'y'
    initialDCM = [1 0 0; 0 0 -1; 0 1 0];
    RotationFunction = @AboutY;
    HeadingFunction = @Yheading;
elseif RotationAxis == 'z'
    initialDCM = [1 0 0; 0 1 0; 0 0 1];
    RotationFunction = @AboutZ;
    HeadingFunction = @Zheading;
elseif RotationAxis == 'x'
    initialDCM = [0 1 0; 0 0 1; 1 0 0];
    RotationFunction = @AboutX;
    HeadingFunction = @Xheading;
else
    error('Rotation axis is improperly defined')
end

% Define set of conditions to simulate
ReactionWheelSpeeds = 1:10:MaxReactionWheelSpeed;
Headings = 1:10:360;
% Matrix of output values
ErrorRMS = zeros(length(Headings),length(ReactionWheelSpeeds));

% Wait bar indicates progress
wait = waitbar(0,'progress');

% indexing
SpeedIndex = 0;

%% Calculate

% For the defined set of reaction wheel speeds 
for i =  ReactionWheelSpeeds
    % Magnetometer uncertainty for the specified speed
    XMagErrorRMS = XMagErrorRMSdata(1,i);
    YMagErrorRMS = YMagErrorRMSdata(1,i);
    ZMagErrorRMS = ZMagErrorRMSdata(1,i);
    
    waitbar(i/MaxReactionWheelSpeed)
    % indexing
    SpeedIndex = SpeedIndex+1;
    HeadingIndex = 0;
    
    % For the defined set of heading values
for Heading = Headings
    % Convert heading to a rotation matrix
    DCM = initialDCM*RotationFunction(Heading/180*3.141592);
    
    % find ambient field vectors in body frame
    BodyAxisGrav = DCM'*InertialGravity;
    BodyAxisMag = DCM'*AmbientMagnetic; 
    
    % indexing
    HeadingIndex = HeadingIndex+1;
    
    % for a large set of measurements
    for Measurement = 1:1:MeasurementNumber
        % Add noise to the magnetometer measurement
        MeasuredMag = BodyAxisMag+[randn*XMagErrorRMS; randn*YMagErrorRMS; randn*ZMagErrorRMS];
        
        % Calculate heading
        MeasuredHeading = HeadingFunction(MeasuredMag, BodyAxisGrav);
        
        % Calculate and log error
        MeasurementError(Measurement) = MeasuredHeading-Heading;
        % Keep error within +- 180 deg
        if abs(MeasurementError(Measurement))> 180
            MeasurementError(Measurement) = (360-abs(MeasurementError(Measurement)))*sign(-MeasurementError(Measurement));
        end
        
    end
    % Calculate and log the RMS of measurement error for the specified
    % reaction wheel speed and heading
    ErrorRMS(HeadingIndex,SpeedIndex) = rms(MeasurementError);
    
end
end

%% plot
figure(1)
mesh(ReactionWheelSpeeds, Headings, ErrorRMS)
title('RMS of measured heading error as a function of true heading and parallel reaction wheel speed')
zlabel('RMS of measured heading error (deg)')
ylabel('Heading')
xlabel('Speed of the parallel reaction wheel')