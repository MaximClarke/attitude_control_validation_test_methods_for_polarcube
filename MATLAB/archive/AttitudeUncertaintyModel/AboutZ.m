% AboutZ.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com
% AboutZ is a function that produces a direction-cosine matrix
% corersponding to a rotation of "Heading" radians about the Body Z axis.
% This function was designed to be used by the AttitudeUncertaintyModel.m
% script because it assumes that rotations will only occur about one of the
% primary body axes.

function [DCM] = AboutZ(Heading)
DCM = [cos(Heading) sin(HeadingC) 0;cos(Heading) -sin(Heading) 0; 0 0 1];

end