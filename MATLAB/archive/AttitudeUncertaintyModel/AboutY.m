% AboutY.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com
% AboutY is a function that produces a direction-cosine matrix
% corersponding to a rotation of "Heading" radians about the Body Y axis.
% This function was designed to be used by the AttitudeUncertaintyModel.m
% script because it assumes that rotations will only occur about one of the
% primary body axes.

function [DCM] = AboutY(Heading)
DCM = [cos(Heading) 0 sin(Heading); 0 1 0; -sin(Heading) 0 cos(Heading)];

end