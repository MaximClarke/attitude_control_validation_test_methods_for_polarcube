% ReactionWheelAliasingModel.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% ReactionWheelAliasingModelFN is an executable version of 
% ReactionWheelAliasingModel that is intended to be used by the AttitudeUncertaintyModel 
% script. It uses a monte-carlo type approach to determine
% the uncertainty in magnetometer measurement that is introduced by  
% sinusoidal disturbances produced by the rotating permagnets in the reaction
% wheel motors. 

% A disturbing signal composed of 3 sinusoids of equal amplitude is sampled 
% at specific frequency over a period and averaged. 
% The mean value of the samples will have an error. Uncertainty
% is characterized by performing this several times with slight variation
% in sample times and sinusoid phase. 

% This script produces a sample set that represents uncertainty as a
% function of the speed of a single reaction wheel for a fixed sample frequency 
% and fixed speed of the secondary reaction wheels.

function [ErrorRMS] = ReactionWheelAliasingModelFN(SampleFrequency, MeasurementPeriod, MeasurementNumber,MaxReactionWheelSpeed, RotationAxis, Xamplitude, Yamplitude, Zamplitude, Xspeed, Yspeed, Zspeed)
%% Set parameters

% This segment helps automate the use of the executing script by letting
% the RotationAxis parameter identify how the input variables are
% implemented

if RotationAxis == 'x'
    PrimaryAmplitude = Xamplitude;
    SecondaryAmplitude1 = Yamplitude;
    SecondarySpeed1 = Yspeed;
    SecondaryAmplitude2 = Zamplitude;
    SecondarySpeed2 = zspeed;
elseif RotationAxis == 'y'
    PrimaryAmplitude = Yamplitude;
    SecondaryAmplitude1 = Xamplitude;
    SecondarySpeed1 = Xspeed;
    SecondaryAmplitude2 = Zamplitude;
    SecondarySpeed2 = Zspeed;
elseif RotationAxis == 'z'
    PrimaryAmplitude = Zamplitude;
    SecondaryAmplitude1 = Xamplitude;
    SecondarySpeed1 = Xspeed;
    SecondaryAmplitude2 = Yamplitude;
    SecondarySpeed2 = Yspeed;
else
    error('RotationAxis is improperly defined')
end





SampleTimeUncertainty = (1/SampleFrequency/20); %std of sample time error

SampleTimes = 1:(SampleFrequency*MeasurementPeriod);
SampleTimes = SampleTimes/SampleFrequency; %s

% empty vectors
MeasurementError = zeros(1,MeasurementNumber);
ErrorRMS = zeros(2,(MaxReactionWheelSpeed+1));

%% Calculation

% Determine uncertainty for the full set of reaction wheel speeds.
for ReactionWheelSpeed = 0:1:MaxReactionWheelSpeed
    
    % Perform several measurements
    for Measurement = 1:1:MeasurementNumber
    % Add noise to sample times    
    NoiseySampleTimes = SampleTimes + randn(1,length(SampleTimes))*SampleTimeUncertainty;
    % Sample a signal comprised of 3 sinusoids with unknown phase and a mean value
    % of 0
    Samples = PrimaryAmplitude *sin(rand + NoiseySampleTimes*ReactionWheelSpeed)+...
        SecondaryAmplitude1*sin(rand + NoiseySampleTimes*SecondarySpeed1)+...
        SecondaryAmplitude2*sin(rand + NoiseySampleTimes*SecondarySpeed2);
    % Calculate and log measurement error
    MeasurementError(Measurement) = mean(Samples);
    end
    % Calculate and log measurement uncerainty
    ErrorRMS(:,ReactionWheelSpeed+1) = [rms(MeasurementError); ReactionWheelSpeed];
end

end