% SheppardMRP.m
% author: Maxim Clarke
% Credit to Carlos Pulido, PolarCube ACS dynamics model
% email: maxaclarke@gmail.com

% SheppardMRP uses Sheppard's method to convert a direction cosine matrix
% into a quaternion, then converts the quaternion into a Modified Rodrigues
% Parameter.

% Sheppard, S.W., “Quaternion from Rotation Matrix,” Journal of Guidance and Control,
% Vol. 1, No. 3, 1978, pp. 223-224.
function sigma  = SheppardMRP(C)
tr = trace(C);
B0s = .25 * (1 + tr);
B1s = .25 * (1 + 2 * C(1,1) - tr);
B2s = .25 * (1 + 2 * C(2,2) - tr);
B3s = .25 * (1 + 2 * C(3,3) - tr);

if B0s > max([B1s,B2s,B3s])
    B0 = sqrt(B0s);
    B1 = (C(2,3) - C(3,2)) / (4 * B0);
    B2 = (C(3,1) - C(1,3)) / (4 * B0);
    B3 = (C(1,2) - C(2,1)) / (4 * B0);
elseif B1s > max([B2s,B3s])
    B1 = sqrt(B1s);
    B0 = (C(2,3) - C(3,2)) / (4 * B1);
    B2 = (C(1,2) + C(2,1)) / (4 * B1);
    B3 = (C(3,1) + C(1,3)) / (4 * B1);
elseif B2s > B3s
    B2 = sqrt(B2s);
    B0 = (C(3,1) - C(1,3)) / (4 * B2);
    B1 = (C(1,2) + C(2,1)) / (4 * B2);
    B3 = (C(2,3) + C(3,2)) / (4 * B2);
else
    B3 = sqrt(B3s);
    B0 = (C(1,2) - C(2,1)) / (4 * B3);
    B1 = (C(3,1) + C(1,3)) / (4 * B3);
    B2 = (C(2,3) + C(3,2)) / (4 * B3);
end

% convert to MRP
sigma(1,1) = B1 / (1 + B0);
sigma(2,1) = B2 / (1 + B0);
sigma(3,1) = B3 / (1 + B0);

% Switch to shadow MRP if necessary
if sqrt(sigma' * sigma) > 1
    sigma = -sigma ./ (sigma' * sigma);
end