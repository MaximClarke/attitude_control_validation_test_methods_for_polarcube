% StringTestbedModel.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% StringTestbedModel.m is a dynamic model of test model motion on the
% string suspension testbed. The test model is subject to spring toreqs and
% damping torques from the string as well as gyroscopic torques from the
% reaction wheels. The test model is slao constrined to motion about the
% local vertical (the rotation axis) torques that are not parallel to the
% roation axis do not affect test model motion.

%% setup

InitialTheta = 0; % Initial deflection from equilibrium angle
TargetTheta = 0.5; % Target deflection from equilibrium angle

% string properties
SpringConstant= -0.00000545;
DampingConstant = 0.0000062;
EquilibriumAngle = 3;

% timestamp
t= 300;
dt = 0.05;
simtime = dt:dt:t;
outputlength = t/dt;

% Moment of inertia of the reaction wheels
J_RW=1.12e-5;

% Moment of inertia of the Test model body
mb = 0.4;
hb = 0.10;
wb = 0.10;
db = 0.33;
IB=[mb*(hb^2+db^2)/12 0 0;
    0 mb*(wb^2+db^2)/12 0;
    0 0 mb*(hb^2+wb^2)/12];

% Define the roation axis in the body frame
BFRotationAxis = [0; 0; 1];
BFRotationAxis = BFRotationAxis/norm(BFRotationAxis);

% Empty vectors
omegaBB = zeros(3,outputlength); % Body rotation rates in the body frame
omegaRW = zeros(3,outputlength); % Reaction wheel speeds
MRP = zeros(3,outputlength);     % Attitude in Modified Rodriguez parameters
MRPdot = zeros(3,outputlength);  % Rate change of MRP
ErrorMRP = zeros(3,outputlength);% ...
theta = zeros(1,outputlength);   % Deflection angle
% torques
Tgyro = zeros(3,outputlength);   
Tdamping = zeros(3,outputlength);
Tcontrol = zeros(3,outputlength);
Tstring = zeros(3,outputlength);
torques = zeros(3,outputlength);

K = diag([0.0003; 0.0003; 0.0003]); % Potential gains
P = diag([0.0005; 0.0005; 0.0005]); % Derivative gains
Ki = diag([30; 30; 30]);% Integral gains



% initial conditions
omegaBB(:,1) = BFRotationAxis*0;
MRP(:,1) = BFRotationAxis*tan(InitialTheta/4);
target_sigma = BFRotationAxis*tan(TargetTheta/4);
theta(:,1) = InitialTheta;
sigInt = [0; 0; 0];
target_omega = [0; 0; 0];
target_omegaDot = [0; 0; 0];
d_omega0 = omegaBB(:,1) - target_omega;


wait = waitbar(0,'progress');
%% Compute

for i=2:length(simtime)
    waitbar(i/length(simtime))
    
    % TOrques from testbed
    Tstring(:,i) = SpringConstant*(theta(i-1))* BFRotationAxis;
    Tdamping(:,i) = -DampingConstant*(omegaBB(:,i-1));
    
    % Error terms
    ErrorMRP(:,i) = diff_sigma(MRP(:,i-1),target_sigma);
    d_omega = omegaBB(:,i-1) - target_omega;
    
    % Error integrator
    sigInt = sigInt + ErrorMRP(:,i-1)*dt;
    zeta = K*sigInt + IB*(d_omega - d_omega0);
    
    % Control law (Motor output torque)
    Tcontrol(:,i) = -IB*(target_omegaDot - CrossMatrix(omegaBB(:,i-1))*target_omega) + K*ErrorMRP(:,i) + P*d_omega ...
             +(P*Ki)*zeta - (CrossMatrix(target_omega) - CrossMatrix(Ki*zeta))*(IB*omegaBB(:,i-1) + J_RW*(omegaRW(:,i-1)+omegaBB(:,i-1)));

    % Sum of torques     
    torques(:,i) = Tstring(:,i)+Tdamping(:,i)-Tcontrol(:,i);
    
    % Rigid body dynamics
    [omegaBB(:,i),MRP(:,i)] = RK4integrate(dt,omegaBB(:,i-1),MRP(:,i-1),omegaRW(:,i-1),torques(:,i),IB,J_RW,BFRotationAxis);
    
    % Integrate reaction wheel speed
    omegaRW(:,i) = omegaRW(:,i-1)+dt/J_RW*Tcontrol(:,i-1);
    
    % Update deflection angle
    theta(i) = 4*atan(norm(MRP(:,i)))*sign(dot(MRP(:,i),BFRotationAxis));
    
end

%% Plot

figure(2)
subplot(3,1,1)
plot(simtime,theta)
title('String testbed model output')
ylabel('Deflectionn angle (rad)')
subplot(3,1,2)
plot(simtime,Tcontrol(3,:))
ylabel('Motor output torque (Nm)')
subplot(3,1,3)
plot(simtime,omegaRW(3,:))
ylabel('reaction wheel speed')
xlabel('Time (s)')





