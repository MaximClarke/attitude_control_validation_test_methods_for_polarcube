
data = dlmread('OscillationTest2.txt');
data(:,8) = data(:,8)-5.1;
data(:,1) = (data(:,1)-data(1,1))/1000;
SpringConstant = -0.0545;
CD = 0.062;
[simtime,theta] = StringTestbedModelFN(data(1,8), SpringConstant, CD , round(data(length(data(:,1)),1)));
[simtime2,theta2] = StringTestbedModelFN(data(1,8), SpringConstant, CD+0.3 , round(data(length(data(:,1)),1)));
for j = 1:length(data(:,1))
    if data(j,8) < (-3.141592)
       data(j,8) = data(j,8)+2*3.141592;
    end
end
figure(1)
subplot(2,1,1)
plot(data(:,1),data(:,8),simtime,theta)
p(2).Color = [0 0.75 0];
title('Measured attitude response (blue) and a damped spring simulation fit to match (green)')
subplot(2,1,2)
plot(simtime2,theta2,'Color',[0,0.75,0])
title('Prediction of attitude response under de-tumble control based on the fitting damped spring model')