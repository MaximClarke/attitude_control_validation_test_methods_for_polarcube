% StringTestbedModel.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% StringTestbedModel.m is a dynamic model of test model motion on the
% string suspension testbed. The test model is subject to spring toreqs and
% damping torques from the string as well as gyroscopic torques from the
% reaction wheels. The test model is slao constrined to motion about the
% local vertical (the rotation axis) torques that are not parallel to the
% roation axis do not affect test model motion.

%% Input parameters
RotationAxis = 'z'; % body axis about which rotation occurs ('x', 'y' or 'z')

t= 45; % (length of propagation)

SampleBinPeriod = 0.25; % seconds
SampleFrequency  = 170; % Hertz

ControlPeriod = 0.1; % seconds
ControlStepSize = ControlPeriod*SampleFrequency;
ControlCounter = round(rand*10);
ControlTorque  = [0; 0; 0];

% ThetaPredictionPeriod = 0.05;
% ThetaPredictionStepSize = ThetaPredictionPeriod*SampleFrequency;
% ThetaPredictionCounter = round(rand*10);

DeterminationPeriod = 0.05;
DeterminationStepSize = DeterminationPeriod*SampleFrequency;
DeterminationCounter = round(rand*10);
ThetaMA = nan;


%Initial test model conditions
InitialTheta = 2.8; % Initial deflection from equilibrium angle
ThetaPC = InitialTheta;
PredictedTheta = InitialTheta;
InitialRWSpeeds = [100+(rand*5-2.5); 100+(rand*5-2.5); 100+(rand*5-2.5)]; % Initial Speeds of the reaction wheels
InitialBodyRate = 0; % initial rotation rate about the rotation axis

% string properties
SpringConstant= 0.0000197; % Nm/rad
DampingConstant = 0.0000155; %Nm/(rad/s)
EquilibriumAngle = 0; % heading that corresponds to the equilibrium angle of the string(rad)

% Confidence in gyro propagation in predictor-corrector filter
PredictorCorrectorGain = 0.98;

% Control law Gains
% With Attitude control
TargetTheta = NaN; % Target deflection from equilibrium angle (defining it as NaN turns control off)
K = diag([0.0018; 0.0018; 0.0002]); % Potential gains
P = diag([0.003; 0.003; 0.0005]); % Derivative gains
Ki = diag([20; 20; 15]);% Integral gains

% Ambient field properties
AmbientMagnetic = [440 0 -630]';  % Ambient magnetic field vector in Earth-reference
InertialGravity = [0 0 -256]';    % Ambient gravity field vector in Earth-reference

% Measured influence of the reaction wheels on magnetoeter measurement
AmplitudeXonX = 244;
AmplitudeXonY = 267;
AmplitudeXonZ = 213;
AmplitudeYonX = 500;
AmplitudeYonY = 770;
AmplitudeYonZ = 349;
AmplitudeZonX = 700;
AmplitudeZonY = 700;
AmplitudeZonZ = 700;

%Gyro Properties
GyroSensitivity = 8.75; %mdps/count
Gyro1SensitivityError = 0; %mdps/count
Gyro2SensitivityError = 0; %mdps/count
Gyro1ZeroRateLevel = [40; 74; 10]; %counts
Gyro2ZeroRateLevel = [-20; 35; 10]; %counts
Gyro1NoiseRMS = [24; 25; 31]; %counts
Gyro2NoiseRMS = [30; 30; 30]; %counts

% Moment of inertia of the reaction wheels
J_RW=1.12e-5;

% Moment of inertia of the Test model body (from CAD)

IB=[0.002106 -1.1e-5 -0.00012;
    -1.1e-5 0.00214 1.58e-5;
    -0.00012 1.58e-5 0.000975];

%% Setup

% Define the orientation of the test model body with respect to
% Earth-reference, the axis of rotation in the body frame and the rotation
% and attitude determination functions to use.
if RotationAxis == 'y'
    initialDCM = [1 0 0; 0 0 -1; 0 1 0];
    BFRotationAxis = [0; 1; 0];
    RotationFunction = @AboutY;
    HeadingFunction = @Yheading;
elseif RotationAxis == 'z'
    initialDCM = [1 0 0; 0 1 0; 0 0 1];
    BFRotationAxis = [0; 0; 1];
    RotationFunction = @AboutZ;
    HeadingFunction = @Zheading;
elseif RotationAxis == 'x'
    initialDCM = [0 1 0; 0 0 1; 1 0 0];
    BFRotationAxis = [1; 0; 0];
    RotationFunction = @AboutX;
    HeadingFunction = @Xheading;
else
    error('Rotation axis is improperly defined')
end

% Normalize the roation axis in the body frame
BFRotationAxis = BFRotationAxis/norm(BFRotationAxis);

% Sensor Sampling Properties
SamplePeriod = 1/SampleFrequency; % seconds
SamplesPerMeasurement = round(SampleFrequency*SampleBinPeriod); % # of samples per measurement
SampleCount = 0;

% timestamp
dt = 1/SampleFrequency; %(timestep at the sample frequency of the magnetometer)
simtime = dt:dt:t; % Timestamp of the simulation
outputlength = t/dt; % useful for defining empty vectors

% Empty vectors
% Test model body motion
omegaBB = zeros(3,outputlength); % Body rotation rates in the body frame
omegaRW = zeros(3,outputlength); % Reaction wheel speeds
MRP = zeros(3,outputlength);     % Attitude in Modified Rodriguez parameters
ErrorMRP = zeros(3,outputlength);% MRP representing attitude error
theta = zeros(1,outputlength);   % Deflection angle

% torques
Tstring = zeros(3,outputlength);
Tdamping = zeros(3,outputlength);
Tcontrol = zeros(3,outputlength);
torques = zeros(3,outputlength);


% initial conditions
% Test model motion
omegaBB(:,1) = BFRotationAxis*InitialBodyRate; % Initial body rate will be about the rotation axis
omegaRW(:,1) = InitialRWSpeeds; % Initial rates of the reaction wheels
MRP(:,1) = BFRotationAxis*tan(InitialTheta/4); % Initial MRP WRT the equilibrium position
theta(:,1) = InitialTheta;
ADnumber = 0;

% Control Law parameters
targetMRP = BFRotationAxis*tan(TargetTheta/4);
sigInt = [0; 0; 0];
target_omega = [0; 0; 0];
target_omegaDot = [0; 0; 0];
d_omega0 = omegaBB(:,1) - target_omega;

% Empty vectors for magnetometer measurements
MeasuredMag = zeros(3,outputlength);
MagnetometerError = [0; 0; 0];

% Empty vectors for determined attitude
MeasuredThetaPeriodic = zeros(1,outputlength);
MeasuredThetaMovingAverage = zeros(1,outputlength);
MeasuredThetaInstantaneous= zeros(1,outputlength);
MeasuredThetaGyro= zeros(1,outputlength);
MeasuredThetaPredictorCorrector= zeros(1,outputlength);

% Initial conditions for determined attitude
MeasuredThetaPeriodic(1) = nan;
MeasuredThetaMovingAverage(1) = nan;
MeasuredThetaGyro(1)= InitialTheta;
MeasuredThetaPredictorCorrector(1)= InitialTheta;

% initial angular position of the reaction wheels 
RWangles = rand(3,1)*2*3.141592;

% Wait bar indicates progress
progressbar;

%% Compute

for i=2:length(simtime)
    
    progressbar(i/length(simtime))
%% Propagate Dynamics
    % Rigid body dynamics
    [omegaBB(:,i),MRP(:,i)] = TestbedRK4integrate(dt,omegaBB(:,i-1),MRP(:,i-1),omegaRW(:,i-1),torques(:,i-1),IB,J_RW,BFRotationAxis);
    
    % Integrate reaction wheel speed
    omegaRW(:,i) = omegaRW(:,i-1)+dt/J_RW*Tcontrol(:,i-1);
    
%% Determine attitude
    
    % Update deflection angle
    theta(i) = 4*atan(norm(MRP(:,i)))*sign(dot(MRP(:,i),BFRotationAxis));
    Heading = EquilibriumAngle+theta(i);

    % Convert heading to a rotation matrix
    DCM = initialDCM*RotationFunction(Heading);
    
    % find ambient field vectors in body frame
    BodyAxisGrav = DCM'*InertialGravity;
    BodyAxisMag = DCM'*AmbientMagnetic; 

    % Calculate magnetometer error by sampling a signal composed of 3
    % sinusoids.
    RWangles = RWangles+(dt+(rand-rand)*dt/10)*omegaRW(:,i-1);
   
    MagnetometerError(1) = AmplitudeXonX *sin(RWangles(1))+...
        AmplitudeYonX*sin(RWangles(2))+...
        AmplitudeZonX*sin(RWangles(3));
    MagnetometerError(2) = AmplitudeXonY *sin(RWangles(1))+...
        AmplitudeYonY*sin(RWangles(2))+...
        AmplitudeZonY*sin(RWangles(3));
    MagnetometerError(3) = AmplitudeXonZ *sin(RWangles(1))+...
        AmplitudeYonZ*sin(RWangles(2))+...
        AmplitudeZonZ*sin(RWangles(3));
    
    % Add error to the magnetometer measurement
    MeasuredMag(:,i) = BodyAxisMag+MagnetometerError;
   
    
    % Once sufficient samples have been taken

    
    % Gyro output
    % Simulate output
     Gyro1Out = omegaBB(:,i)/3.141592*180*1000/(GyroSensitivity+Gyro1SensitivityError)+ Gyro1ZeroRateLevel + Gyro1NoiseRMS.*randn(3,1);
     Gyro2Out = omegaBB(:,i)/3.141592*180*1000/(GyroSensitivity+Gyro2SensitivityError)+ Gyro2ZeroRateLevel + Gyro2NoiseRMS.*randn(3,1);
     
     MeanGyroOut = (Gyro1Out+Gyro2Out)/2;
    
     % Calculate rate from output
     MeasuredOmegaAboutAxis = dot(BFRotationAxis,MeanGyroOut)/1000/180*3.141592*GyroSensitivity;
    
     % Direct integration of Gyro output 
     MeasuredThetaGyro(i) = MeasuredThetaGyro(i-1)+dt*MeasuredOmegaAboutAxis;
     
     
    % Predictor-corrector calculation of theta
    % If first direct attitude measurement has yet to arrive, propagate
    % gyro
   if DeterminationCounter > DeterminationStepSize
        if i > SamplesPerMeasurement
        % Calculate a moving average of the magneotmeter data
       
        MovingAverageMagnetometerMeasurement = mean(MeasuredMag(:,(i-SamplesPerMeasurement):i),2);
    
      % Measure MRP directly from sensor data rather than heading
%     MRPMovingAverage = MRPfromTRIAD(MovingAverageMagnetometerMeasurement,BodyAxisGrav);
%     MeasuredThetaMovingAverage(i) = 4*atan(norm(MRPMovingAverage))*sign(dot(MRPMovingAverage,BFRotationAxis))-EquilibriumAngle;

        ThetaMA = HeadingFunction(MovingAverageMagnetometerMeasurement, BodyAxisGrav)-EquilibriumAngle;  
        PredictedTheta = ThetaPC + MeasuredOmegaAboutAxis * DeterminationCounter*dt;
        if PredictedTheta*ThetaMA < 0
            ThetaPC = PredictedTheta;
        else
            
        ThetaPC = PredictedTheta*PredictorCorrectorGain+ThetaMA*(1-PredictorCorrectorGain);
        
        ADnumber = ADnumber+1;
        ADdata(:,ADnumber) = [ThetaMA; ThetaMA-theta(i); ThetaPC; ThetaPC-theta(i); simtime(i)];
        %ADdata(:,ADnumber) = [ThetaMA; ThetaMA-theta(i-round(SampleFrequency*SampleBinPeriod/2)); ThetaPC; ThetaPC-theta(i-round(SampleFrequency*SampleBinPeriod/2)); simtime(i)-(SampleBinPeriod/2)];

        end
        else
    % If insufficient data has been collected, do not record data
        ThetaPC = MeasuredThetaGyro(i);
        ThetaMA = nan;  
        end
        DeterminationCounter = 0;
    end
   
    
    DeterminationCounter = DeterminationCounter+1;
    
    % Calculate heading
    %MeasuredThetaInstantaneous(i) = HeadingFunction(MeasuredMag(:,i), BodyAxisGrav)-EquilibriumAngle;
    MeasuredThetaMovingAverage(i) = ThetaMA;
    MeasuredThetaPredictorCorrector(i) = ThetaPC;
    
    % Obtain attitude and body rates from sensor measurements
    MeasuredMRP = BFRotationAxis * tan(MeasuredThetaPredictorCorrector(i)/4);
    omegaBBfromGyro = BFRotationAxis * MeasuredOmegaAboutAxis;
    
%% Attitude control
    
    % Error terms
    ErrorMRP(:,i) = diff_sigma(MeasuredMRP,targetMRP); % MRP of attitude error
    d_omega = omegaBBfromGyro - target_omega; % error in body rate
    
    % Error integrator
    sigInt = sigInt + ErrorMRP(:,i)*dt;
    zeta = K*sigInt + IB*(d_omega - d_omega0);
       
    % Control law (Motor output torque)(Nm) Hogan Schaub 2013
    if isnan(ErrorMRP(:,i))
    elseif ControlCounter >= ControlStepSize 
    ControlTorque = -IB*(target_omegaDot - CrossMatrix(omegaBBfromGyro)*target_omega) + K*ErrorMRP(:,i) + P*d_omega + P*Ki*zeta;%...
       % - CrossMatrix(omegaBBfromGyro)*(IB*omegaBBfromGyro + J_RW*(omegaRW(:,i)+omegaBBfromGyro));
    ControlCounter = 0;
    end
    for j = 1:3
    if abs(ControlTorque(j)) > 0.00025
        ControlTorque(j) = 0.00025*sign(ControlTorque(j));
    end
    end
    
    
    ControlCounter = ControlCounter + 1;
    
    Tcontrol(:,i) = ControlTorque;
    
%% Testbed dynamics
    % Torques from testbed (Nm)
    Tstring(:,i) = -SpringConstant*theta(i)* BFRotationAxis;
    Tdamping(:,i) = -DampingConstant*omegaBB(:,i);
    
    % Sum of torques     
    torques(:,i) = Tstring(:,i)+Tdamping(:,i)-Tcontrol(:,i);
end

%% Plot

figure(1)
subplot(3,1,1)
plot(simtime,Tcontrol(1,:))
title('Motor output torques')
ylabel('x (Nm)')
subplot(3,1,2)
plot(simtime,Tcontrol(2,:))
ylabel('y (Nm)')
subplot(3,1,3)
plot(simtime,Tcontrol(3,:))
ylabel('z (Nm)')
xlabel('Time (s)')

figure(2)
subplot(3,1,1)
plot(simtime,omegaRW(1,:))
title('reaction wheel speeds')
ylabel('x (rad/s)')
subplot(3,1,2)
plot(simtime,omegaRW(2,:))
ylabel('y (rad/s)')
subplot(3,1,3)
plot(simtime,omegaRW(3,:))
ylabel('z (rad/s)')
xlabel('Time (s)')

targetthetas = ones(1,length(simtime))*TargetTheta;

figure(3)
subplot(2,1,1)
plot(ADdata(5,:), ADdata(1,:),'g',simtime, MeasuredThetaGyro,'m',ADdata(5,:), ADdata(3,:), simtime,theta, simtime,targetthetas)
title('Atttide determiation compared to true atttiude')
ylabel('Deflection angle (rad)')
legend('AD through moving average filter','AD through propagating gyros','AD through predictor-corrector filter','True deflection angle','Atttiude target')
subplot(2,1,2)
plot( ADdata(5,:), ADdata(2,:),'g',ADdata(5,:), ADdata(4,:))
ylabel('Attitude determination error (rad)')
xlabel('Time (s)')


