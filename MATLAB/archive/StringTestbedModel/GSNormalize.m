% GSNormalize.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% GSNormalize.m uses the Gram-Schmidt process to orthonormalize a
% direction-cosine matrix.

% Input matrix is A (mst be 3x3) 
% p,q,r identifies the order of operations
function [ out ] = GSNormalize( A,p,q,r )

out=zeros(3,3);
% normalize first vector
out(:,p) = A(:,p)/norm(A(:,p));
% orthogonalize second vector
out(:,q) = A(:,q)-dot(A(:,p),A(:,q))/dot(A(:,p),A(:,p))*A(:,p);
% normalize second vector
out(:,q) = out(:,q)/norm(A(:,q));
% orthogonalize third vector
out(:,r) = A(:,r)-dot(A(:,p),A(:,r))/dot(A(:,p),A(:,p))*A(:,p)-dot(A(:,q),A(:,r))/dot(A(:,q),A(:,q))*A(:,q);
% normalize third vector
out(:,r) = out(:,r)/norm(A(:,r));
end

