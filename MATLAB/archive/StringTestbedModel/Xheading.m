% Xheading.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% Xheading.m calculates heading of the body y axis with respect to magnetic 
% north provided readings from a magnetometer and
% accelerometer. Xheading only works if the X body axis is pointed above
% the horizontal

function [Heading] = Xheading(MagFieldVector, GravityVector)
% Calculate magnetic west
DueWest = cross(MagFieldVector, GravityVector);
DueWest = DueWest/norm(DueWest);

% Cross product of the y body axis and gravity
YcrossGrav = cross([0 1 0], GravityVector);
YcrossGrav = YcrossGrav/norm(YcrossGrav);

% Calculate heading
Heading = acos(dot(DueWest, YcrossGrav));
% Positive or neagitve angle (This is the reason you need 3 heading
% formulas)
if DueWest(2) < 0
    Heading = -Heading;
end

end
    