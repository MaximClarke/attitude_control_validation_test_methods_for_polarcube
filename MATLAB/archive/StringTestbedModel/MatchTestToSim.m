% MatchTestToSim.m
% author: Maxim Clarke 
% email: maxaclarke@gmail.com

% MatchTestToSim.m plots four sets of test data to output of the
% StringTestbedModelFN function. This is used to find the parameters that
% provide a best fit of the string testbed model to the measured test data.
NumberOfDataSets = 4;
HeadingColumn = 8;
TimeColumn = 1;
Prediction = true;

% Set equilibrium angle spring constant and damping constant
SpringConstant = 0.00000545;
DampingConstant = 0.0000062;
EquilibriumAngle = 5.1;

% Moment of inertia of the Test model body
mb = 0.4;
hb = 0.10;
wb = 0.10;
db = 0.33;
IB=[mb*(hb^2+db^2)/12 0 0;
    0 mb*(wb^2+db^2)/12 0;
    0 0 mb*(hb^2+wb^2)/12];

K_pred = diag([0; 0; 0]);
P_pred = diag([0; 0; 0]);
Ki_pred = diag([0; 0; 0]);

K_fit = diag([0; 0; 0]);
P_fit = diag([0; 0; 0]);
Ki_fit = diag([0; 0; 0]);

TargetTheta = 0;

% For the set of test data files
for i = 1:NumberOfDataSets
    % collect test data
    data = dlmread(['C:\Users\Maxim\Documents\Thesis\Git\MATLAB\DataLogs\OscillationTests\OscillationTest' num2str(i) '.txt']);
    % Apply equilibrium angle
    DeflectionAngles = data(:,HeadingColumn)-EquilibriumAngle;
    % Correct timestamp for bias and scale
    TimeStamps = (data(:,TimeColumn)-data(1,TimeColumn))/1000;

    % Collect string testbed model output 
    [simtime_fit,theta_fit] = StringTestbedModelFN(DeflectionAngles(1),TargetTheta,K_fit,P_fit,Ki_fit,IB,SpringConstant, DampingConstant , round(TimeStamps(length(TimeStamps))));
    
    if Prediction == true
        [simtime_pred,theta_pred] = StringTestbedModelFN(DeflectionAngles(1),TargetTheta,K_pred,P_pred,Ki_pred,IB,SpringConstant, DampingConstant , round(TimeStamps(length(TimeStamps))));
    else
        simtime_pred = NaN;
        theta_pred = NaN;
    end
    % Keep test data within +-pi
    for j = 1:length(data(:,1))
        if DeflectionAngles(j) < (-3.141592)
       DeflectionAngles(j) = DeflectionAngles(j)+2*3.141592;
        elseif DeflectionAngles(j) > (3.141592)
       DeflectionAngles(j) = DeflectionAngles(j)-2*3.141592;
        end
    end
    % Plot test data and model output
    figure(4)
    subplot(NumberOfDataSets,1,i)
    plot(TimeStamps,DeflectionAngles,simtime_fit,theta_fit,simtime_pred,theta_pred)
    TitleString = 'Heading measured from tests (Blue) plotted against best fitting model of string dynaics (Green)';
    if Prediction == true
        PredictionString = 'and a prediction based on control law configuration (Red)';
    else
        PredictionString = '';
    end
    if i == 1
        title({TitleString;PredictionString})
    end
    if i == NumberOfDataSets
        xlabel('Time (s)')
    end
    ylabel('Heading (rad)')
end