%Reaction Wheel Speed Control

%% Setting Up Transfer Function

%Reaction Wheel + Motor Constants
J_wheel = 1.12e-5;            %Moment of Inertia [kg*m^2]
Kp = 1;                 %Proportional Gain
Kd = 0;                 %Derivative Gain
Ki = 1;                 %Integral Gain
Km = .000305;          %Back-EMF Constant [V/rpm] for 12V motor (from datasheet)
R = 59;                 %Armature Resistance [Ohms] from datasheet
L = 187e-6;             %Terminal Inductance [H] from datasheet

%Closed loop PD Transfer Function

%Finding Best Gains
Kp_vec = 0:.001:0.015;
Kd_vec = 0:.001:0.02;
Ki_vec = 0:.001:0.02;


j = 1;
O_tol = 1.05;
settle_time = 1;   %[s]
%Closed loop system
%Finding best gain combinations
for Kp = Kp_vec
    for Kd = Kd_vec
        for Ki = Ki_vec
            %Numerator
            num = [Kp Ki];

            %Denominator
            d3 = L*J;
            d2 = (R*J) + (b*L) + Kd;
            d1 = (Km^2) + Kp;
            d0 = Ki;
            den = [d3 d2 d1 d0];
            sysTF =	tf(num,den);
            %Step response
            [O,t] = step(sysTF);

            k = find(.99 < O & O <1.01);

            if isempty(k) == 0
                if max(O) < O_tol && t(k(end)) < settle_time
                    Kd_better(j) = Kd;
                    Kp_better(j) = Kp;
                    Ki_better(j) = Ki;
                    settle(j) = max(t);
                    disp(j) = max(O);
                    j = j+1;
                end
            end
        end
    end
end

g = min(settle);
h = find(settle == g);

if  length(h) > 1
    r = min(disp(h));
    q = find(r == disp);
    best_gains = [Kd_best(q), Kp_best(q), Ki_best(q)];
else
    best_gains = [Kd_best(h), Kp_best(h), Ki_best(h)];
end

%% Speed Control

t = 1;
omega_int = 0;
omega_total = 0;

% while(1)
% 
% %Time difference between readings is a constant
% delta_t = .0167;                 %[m]
% 
% %Get Reaction Wheel Speed from interrupt
% omega = 10;                  %[rpm]
% 
% %Get desired reaction wheel speed from Attitude Determination
% omega_r = 20;                      %[rpm]
% 
% %Calculate derivative
% alpha = (omega_previous + omega)/delta_t;   %[rot/m^2]
% 
% %Calculate integral
% omega_int = (omega_r - omega)*delta_t;
% omega_total = omega_int + omega_total;
% 
% %Voltage based on desired speed compared to current speed
% V_in = Kp*(omega_r - omega) + Kd*alpha + Ki*omega_total;
% 
% end
