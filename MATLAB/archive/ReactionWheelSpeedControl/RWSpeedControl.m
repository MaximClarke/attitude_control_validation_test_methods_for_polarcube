
PropTime = 80; %s
PropRate  = 300; %Hz
dt = 1/PropRate;
J_RW=1.12e-5;
time = 0:dt:PropTime;

ControlRate = 3; % Hz
ControlCounterLimit = PropRate/ControlRate;

Deadband = 4;
MaxTorque = 0.00025;

InitialSpeed = 0;
TargetSpeed = 500;

Kp = 0.00004;
Kd = 0;
Ki = 0;

Speed = zeros(1,length(time));
ControlTorque = zeros(1,length(time));
TachometerCount = 0;
TControl = 0;
IntegralTerm = 0;
ControlCounter = 0;
MeasuredSpeed = InitialSpeed;

for i = 2:1:length(time)
    
    Speed(i) = Speed(i-1) + dt * TControl / J_RW;
    
    TachometerCount = TachometerCount + (Speed(i)+Speed(i-1))/2 * dt;
    
    if ControlCounter >= ControlCounterLimit
    
    LastMeasuredSpeed = MeasuredSpeed;
    MeasuredSpeed = round(TachometerCount)/(ControlCounterLimit*dt);
        
    SpeedError = MeasuredSpeed - TargetSpeed ;
    
    IntegralTerm = IntegralTerm + SpeedError * dt;
    
    Accel = (MeasuredSpeed-LastMeasuredSpeed)/(ControlCounterLimit*dt);
    
%     TControl = -Kp*SpeedError - Kd*Accel - Ki*IntegralTerm;
%     
%     if abs(TControl) > 0.0003
%         TControl = 0.0003*sign(TControl);
%     end
    if abs(SpeedError) > Deadband
        TControl = -MaxTorque*sign(SpeedError);
    else
        TControl = 0;
    end
        
        
    ControlCounter = 0;
    TachometerCount = 0;
    end
    
    ControlCounter = ControlCounter + 1;
    
    ControlTorque(i) = TControl;
    
end


figure(1)
subplot(2,1,1)
plot(time, Speed)
subplot(2,1,2)
plot(time, ControlTorque)