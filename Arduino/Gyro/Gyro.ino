#include <Wire.h> //I2C Arduino Library
#include <SPI.h>
#include <SD.h>
#include <SoftwareSerial.h>
SoftwareSerial XBee(0, 1);

#define GYROaddress 0x68 //01101000b, I2C 7bit address of A3G4250D

char logFileName[16];

const int chipSelect = 11 ;
int GYROx, GYROy, GYROz; //triple axis data
boolean NoCard = false; // indicates whether there is an SD card present

void setup() {
  //Initialize Serial communications
    XBee.begin(115200);
  Serial.begin(9600);
  
  //Configure Gyro
  Wire.begin();
  Wire.beginTransmission(GYROaddress);
  Wire.write(0x20);
  Wire.write(0x0F); // Turn measurement on
  Wire.endTransmission();

  
  Serial.print("Initializing SD card...");
  pinMode(chipSelect, OUTPUT);
  
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    NoCard = true;
  }
  else {
    Serial.println("card initialized.");
    SDCardInit(); //opens a new log file on the SD card and checks it.
  }
}

void loop() {
  
 unsigned long timeStamp = millis();
  
  Wire.beginTransmission(GYROaddress);
  Wire.write(0x28 | (1<<7)); //select register 3, X LSB register, pull MSB high to instruct it to read following registers.
  Wire.endTransmission();

  //Read data from each axis, 2 registers per axis
  Wire.requestFrom(GYROaddress, 6);
  if (6 <= Wire.available()) {
    GYROx = Wire.read(); //X lsb
    GYROx |= Wire.read() << 8; //X msb
    GYROy = Wire.read(); //Y lsb
    GYROy |= Wire.read() << 8; //Y msb
    GYROz = Wire.read(); //Z lsb
    GYROz |= Wire.read() << 8; //Z msb
  }
// Check if serial port is open
if(Serial){
  //Print out values of each axis
  Serial.print(timeStamp);
  Serial.print("\t");
  Serial.print(GYROx);
  Serial.print("\t");
  Serial.print(GYROy);
  Serial.print("\t");
  Serial.println(GYROz);
  }
  
  // check if SD card is present
  if(NoCard == false){
    File dataFile = SD.open(logFileName, FILE_WRITE);
    //Print out values of each axis
    dataFile.print(timeStamp);
    dataFile.print("\t");
    dataFile.print(GYROx);
    dataFile.print("\t");
    dataFile.print(GYROy);
    dataFile.print("\t");
    dataFile.println(GYROz);
    dataFile.close();
}}
