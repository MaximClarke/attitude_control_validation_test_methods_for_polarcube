#include <Wire.h> //I2C Arduino Library
#include <math.h> 
#include <SPI.h>
#include <SD.h>
#include <SoftwareSerial.h>
SoftwareSerial XBee(0, 1);

char logFileName[16];

#define MAGaddress 0x1E //0011110b, I2C 7bit address of HMC58834
#define ACCaddress 0x53 //01010011b, I2C 7bit address of ADXL345
#define GYROaddress 0x68 //01101000b, I2C 7bit address of A3G4250D

const int chipSelect = 11 ;
int MAGx, MAGy, MAGz, ACCx, ACCy, ACCz, GYROx, GYROy, GYROz; //triple axis data
unsigned long Timestamp = 0;
unsigned long PreviousTimestamp;
float magnitude,MeasuredHeading, PredictedHeading, PCHeading, Pitch, Roll, dueWEST[3], XcrossGRAV[3], adjMAGx, adjMAGy, adjMAGz, GYROdotGRAV;
boolean NoCard = false; // indicates whether there is an SD card present

void setup() {
  //Initialize Serial communications
  XBee.begin(115200);
  Serial.begin(9600);
  
  //Configure Magnetoemter 
  Wire.begin();
  Wire.beginTransmission(MAGaddress);
  Wire.write(0x00);
  Wire.write(0x70); // 15 hz 8-averaging
  Wire.endTransmission();
  Wire.beginTransmission(MAGaddress);
  Wire.write(0x01); //select gain register
  Wire.write(0x00); // .88 Ga
  Wire.endTransmission();
  Wire.beginTransmission(MAGaddress);
  Wire.write(0x02); //select mode register
  Wire.write(0x00); //continuous measurement mode
  Wire.endTransmission();

  //Configure Accelerometer
  Wire.beginTransmission(ACCaddress);
  Wire.write(0x38); //
  Wire.write(0x00); //Bypass FIFO
  Wire.endTransmission();
  Wire.beginTransmission(ACCaddress);
  Wire.write(0x31); 
  Wire.write(0x08); //full resolution mode
  Wire.endTransmission();
  Wire.beginTransmission(ACCaddress);
  Wire.write(0x2D); 
  Wire.write(0x08); // Turn measurement on
  Wire.endTransmission();
  
  Wire.beginTransmission(GYROaddress);
  Wire.write(0x20);
  Wire.write(0x0F); // Turn measurement on
  Wire.endTransmission();

  Serial.print("Initializing SD card...");
  pinMode(chipSelect, OUTPUT);
  
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    NoCard = true; // no SD card present
  }
  else {
    Serial.println("card initialized.");
    SDCardInit(); //opens a new log file on the SD card and checks it.
  }
}

void loop() {
  PreviousTimestamp = Timestamp;
  Timestamp = millis();
  

  //Read magnetometer output
  Wire.beginTransmission(MAGaddress);
  Wire.write(0x03); //select register 3, X MSB register
  Wire.endTransmission();

  //Read data from each axis, 2 registers per axis
  Wire.requestFrom(MAGaddress, 6);
  if (6 <= Wire.available()) {
    MAGx = Wire.read() << 8; //X msb
    MAGx |= Wire.read(); //X lsb
    MAGz = Wire.read() << 8; //Z msb
    MAGz |= Wire.read(); //Z lsb
    MAGy = Wire.read() << 8; //Y msb
    MAGy |= Wire.read(); //Y lsb
  }
  
  //Correct for bias and gains as determined by SphereCenterCalculation.m
  MAGx = MAGx -105;
  MAGy = MAGy + 184;
  MAGz = MAGz +48;
  
  adjMAGx = MAGx;
  adjMAGy = MAGy;
  adjMAGz = MAGz;


  // Read accelerometer output
  Wire.beginTransmission(ACCaddress);
  Wire.write(0x32); //select register 3, X LSB register
  Wire.endTransmission();

  //Read data from each axis, 2 registers per axis
  Wire.requestFrom(ACCaddress, 6);
  if (6 <= Wire.available()) {
    ACCx = Wire.read(); //X lsb
    ACCx |= Wire.read() << 8; //X msb
    ACCy = Wire.read(); //Z lsb
    ACCy |= Wire.read() << 8; //Z msb
    ACCz = Wire.read(); //Y lsb
    ACCz |= Wire.read() << 8; //Y msb
  }
  
  Wire.beginTransmission(GYROaddress);
  Wire.write(0x28 | (1<<7)); //select register 3, X LSB register, pull MSB high to instruct it to read following registers.
  Wire.endTransmission();

  //Read data from each axis, 2 registers per axis
  Wire.requestFrom(GYROaddress, 6);
  if (6 <= Wire.available()) {
    GYROx = Wire.read(); //X lsb
    GYROx |= Wire.read() << 8; //X msb
    GYROy = Wire.read(); //Y lsb
    GYROy |= Wire.read() << 8; //Y msb
    GYROz = Wire.read(); //Z lsb
    GYROz |= Wire.read() << 8; //Z msb
  }

 float GRAVvec[3] = { -ACCx, -ACCy, -ACCz}; // gravity field vector

  // Calculate magnetic west
  dueWEST[0] = adjMAGy * GRAVvec[2] - adjMAGz * GRAVvec[1];
  dueWEST[1] = adjMAGz * GRAVvec[0] - adjMAGx * GRAVvec[2];
  dueWEST[2] = adjMAGx * GRAVvec[1] - adjMAGy * GRAVvec[0];
  
  //Normalize
  magnitude = sqrt(sq(dueWEST[0]) + sq(dueWEST[1]) + sq(dueWEST[2]));

  dueWEST[0] /= magnitude;
  dueWEST[1] /= magnitude;
  dueWEST[2] /= magnitude;

  magnitude = sqrt(sq(GRAVvec[0]) + sq(GRAVvec[1]) + sq(GRAVvec[2]));

  GRAVvec[0] /= magnitude;
  GRAVvec[1] /= magnitude;
  GRAVvec[2] /= magnitude;
  // cross product of body x axis and gravity vector
  XcrossGRAV[0] = 0;
  XcrossGRAV[1] = -GRAVvec[2];
  XcrossGRAV[2] = GRAVvec[1];

  magnitude = sqrt(sq(XcrossGRAV[1]) + sq(XcrossGRAV[2]));
  
  // Calculate pitch
  Pitch = acos(magnitude);

  if ((GRAVvec[0]) > 0) {
    Pitch = -Pitch;
  }

  // Normalize
  XcrossGRAV[1] /= magnitude;
  XcrossGRAV[2] /= magnitude;

 PredictedHeading = PCHeading+(Timestamp-PreviousTimestamp)*GYROdotGRAV*0.0000001527;

  // Calculate MeasuredHeading
 MeasuredHeading = acos(dueWEST[1] * XcrossGRAV[1] + dueWEST[2] * XcrossGRAV[2]);
 //MeasuredHeading = MeasuredHeading - (+0.000565*pow(MeasuredHeading,4)-0.00097*pow(MeasuredHeading,3)-0.000943*pow(MeasuredHeading,2)+0.0097*MeasuredHeading-0.0088);
  if (dueWEST[0] < 0) {
    MeasuredHeading = -MeasuredHeading;
  }
  
  if (PredictedHeading*MeasuredHeading < 0){
  PCHeading = PredictedHeading;
 }
 else{
 PCHeading = PredictedHeading*0.8+MeasuredHeading*0.2;
}
if (PCHeading > 3.141592){
 PCHeading = PCHeading-6.2831;
}
if (PCHeading < -3.141592){
  PCHeading = PCHeading+6.2831;
}

GYROdotGRAV = -(GYROx*GRAVvec[0]+GYROy*GRAVvec[1]+GYROz*GRAVvec[2]);

  
  //Calculate roll
 Roll = acos(XcrossGRAV[1]);

  if (XcrossGRAV[2] < 0) {
    Roll = -Roll;
  }


  // check if SD card is present
  if(NoCard == false){
    File dataFile = SD.open(logFileName, FILE_WRITE);
    
    //Write data to SD card
    dataFile.print(Timestamp);
    dataFile.print("\t");
    dataFile.print(MAGx);
    dataFile.print("\t");
    dataFile.print(MAGy);
    dataFile.print("\t");
    dataFile.print(MAGz);
    dataFile.print("\t");
    dataFile.print(ACCx);
    dataFile.print("\t");
    dataFile.print(ACCy);
    dataFile.print("\t");
    dataFile.print(ACCz);
    dataFile.print("\t");
    dataFile.print(GYROx);
    dataFile.print("\t");
    dataFile.print(GYROy);
    dataFile.print("\t");
    dataFile.print(GYROz);
    dataFile.print("\t");
    dataFile.print(PCHeading);
    dataFile.print("\t");
    dataFile.print(Pitch, 2);
    dataFile.print("\t");
    dataFile.println(Roll, 2);
    dataFile.close();
  }
  
  // Check if serial port is open
  if(Serial){
  //Write data to SD card
  Serial.print(Timestamp);
  Serial.print("\t");
  Serial.print(MAGx);
  Serial.print("\t");
  Serial.print(MAGy);
  Serial.print("\t");
  Serial.print(MAGz);
  Serial.print("\t");
  Serial.print(ACCx);
  Serial.print("\t");
  Serial.print(ACCy);
  Serial.print("\t");
  Serial.print(ACCz);
  Serial.print("\t");
  Serial.print(GYROx);
  Serial.print("\t");
  Serial.print(GYROy);
  Serial.print("\t");
  Serial.print(GYROz);
  Serial.print("\t");
  Serial.print(PCHeading);
  Serial.print("\t");
  Serial.print(Pitch);
  Serial.print("\t");
  Serial.println(Roll);
  }
  
  //write to XBee
  XBee.print(Timestamp);
  XBee.print("\t");
  XBee.print(MAGx);
  XBee.print("\t");
  XBee.print(MAGy);
  XBee.print("\t");
  XBee.print(MAGz);
  XBee.print("\t");
  XBee.print(ACCx);
  XBee.print("\t");
  XBee.print(ACCy);
  XBee.print("\t");
  XBee.print(ACCz);
  XBee.print("\t");
  XBee.print(GYROx);
  XBee.print("\t");
  XBee.print(GYROy);
  XBee.print("\t");
  XBee.print(GYROz);
  XBee.print("\t");
  XBee.print(PCHeading);
  XBee.print("\t");
  XBee.print(Pitch);
  XBee.print("\t");
  XBee.println(Roll);
}



