#include <Wire.h> //I2C Arduino Library
#include <SPI.h>
#include <SD.h>
#include <SoftwareSerial.h>
SoftwareSerial XBee(0, 1);

#define MAGaddress 0x1E //0011110b, I2C 7bit address of HMC58834

char logFileName[16];

const int chipSelect = 11 ;
int MAGx, MAGy, MAGz; //triple axis data
boolean NoCard = false;

void setup() {
  //Initialize Serial communications
    XBee.begin(115200);
  Serial.begin(9600);
  
  //Configure Magnetoemter 
  Wire.begin();
  Wire.beginTransmission(MAGaddress);
  Wire.write(0x00);
  Wire.write(0x78); //75 hz 1-averaging
  Wire.endTransmission();
  Wire.beginTransmission(MAGaddress);
  Wire.write(0x01); //select gain register
  Wire.write(0x00); // .88 Ga
  Wire.endTransmission();
  Wire.beginTransmission(MAGaddress);
  Wire.write(0x02); //select mode register
  Wire.write(0x00); //continuous measurement mode
  Wire.endTransmission();
  
  Serial.print("Initializing SD card...");
  pinMode(chipSelect, OUTPUT);
  
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    NoCard = true;
  }
  else {
    Serial.println("card initialized.");
    SDCardInit(); //opens a new log file on the SD card and checks it.
  }
}

void loop() {
  
 unsigned long Timestamp = millis();
  
  Wire.beginTransmission(MAGaddress);
  Wire.write(0x03); //select register 3, X MSB register
  Wire.endTransmission();

  //Read data from each axis, 2 registers per axis
  Wire.requestFrom(MAGaddress, 6);
  if (6 <= Wire.available()) {
    MAGx = Wire.read() << 8; //X msb
    MAGx |= Wire.read(); //X lsb
    MAGz = Wire.read() << 8; //Z msb
    MAGz |= Wire.read(); //Z lsb
    MAGy = Wire.read() << 8; //Y msb
    MAGy |= Wire.read(); //Y lsb
  }

// Check if serial port is open
if(Serial){
  //Print out values of each axis
  Serial.print(Timestamp);
  Serial.print("\t");
  Serial.print(MAGx);
  Serial.print("\t");
  Serial.print(MAGy);
  Serial.print("\t");
  Serial.println(MAGz);
  }
  
    // check if SD card is present
  if(NoCard == false){
    File dataFile = SD.open(logFileName, FILE_WRITE);

    dataFile.print(Timestamp);
    dataFile.print("\t");
    dataFile.print(MAGx);
    dataFile.print("\t");
    dataFile.print(MAGy);
    dataFile.print("\t");
    dataFile.println(MAGz);
    dataFile.close();
}
    
  XBee.print(Timestamp);
  XBee.print("\t");
  XBee.print(MAGx);
  XBee.print("\t");
  XBee.print(MAGy);
  XBee.print("\t");
  XBee.print(MAGz);
  XBee.print("\t");
  XBee.print(0);
  XBee.print("\t");
  XBee.print(0);
  XBee.print("\t");
  XBee.print(0);
  XBee.print("\t");
  XBee.print(0);
  XBee.print("\t");
  XBee.print(0);
  XBee.print("\t");
  XBee.print(0);
  XBee.print("\t");
  XBee.print(0);
  XBee.print("\t");
  XBee.print(0);
  XBee.print("\t");
  XBee.println(0);
  
}
